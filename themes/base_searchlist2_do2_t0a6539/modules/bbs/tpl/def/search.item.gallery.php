<?php
/**
 * Список объявлений: вид галерея
 * @var $this BBS
 * @var $item array данные объявления
 * @var $opts array доп. параметры
 */
 $lng_quick = _te('bbs', 'срочно');

 \HTML::attributeAdd($opts['attr'], 'class', 'sr-page__gallery__item sr-page__gallery__item__v2 rel span4');
 if ($item['svc_marked']) {
     \HTML::attributeAdd($opts['attr'], 'class', 'sr-page__item_selected');
 }

 $address = [];
 if ( ! empty($item['city_title'])) {
    $address[] = $item['city_title'];
 }
 if ( ! empty($item['district_title'])) {
    $address[] = $item['district_title'];
 }
 if ( ! empty($opts['showAddr']) && ! empty($item['addr_addr'])) {
    $address[] = $item['addr_addr'];
 }
?>

<div<?= \HTML::attributes($opts['attr']); ?>>
  <div class="sr-page__gallery__item__v2__in">
    <div class="sr-page__item__fav">
        <a href="javascript:void(0);" class="item-fav j-tooltip j-i-fav <?php if ($item['fav']) { ?>active<?php } ?>" data="{id:<?= $item['id'] ?>}" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="<?= ( $item['fav'] ? _te('bbs', 'Удалить из избранного') : _te('bbs', 'Добавить в избранное') ); ?>">
            <i class="fa <?php if ($item['fav']) { ?>fa-star<?php } else { ?>fa-star-o<?php } ?> j-i-fav-icon"></i>
        </a>
    </div>

    <?php if ($item['imgs']): # image ?>
    <a class="sr-page__item__l__in" href="<?= $item['link'] ?>">
        <span class="sr-page__item__l__pic">
            <?php if($item['svc_quick']) { ?>
            <span class="label-lot label_top hidden-phone"><?= $lng_quick ?></span>
            <?php } ?>
            <img src="<?= $item['img_m'] ?>" alt="" />
            <?php if ($item['imgs'] > 1) { ?>
            <span class="sr-page__item__pic__counter">
                <?= $item['imgs'] ?>
            </span>
            <?php } ?>
        </span>
    </a>
    <?php else: ?>
    <a class="sr-page__item__l__in sr-page__item__l__in_empty__pic" href="<?= $item['link'] ?>">
        <?php if($item['svc_quick']) { ?>
            <span class="label-lot label_top hidden-phone"><?= $lng_quick ?></span>
        <?php } ?>
        <img src="<?= $item['img_m:svg'] ?>" alt="" />
    </a>
    <?php endif; ?>

    <div class="sr-page__gallery__item_descr">
        <div class="sr-page__item__category_text">
            <?php if($item['svc_quick']) { ?>
            <span class="label label-warning quickly visible-phone"><?= $lng_quick ?></span>
            <?php } ?>
            <?php if($item['svc_fixed']) { ?>
            <span class="sr-page__item__top__label"><?= _te('bbs', 'топ') ?></span>
            <?php } ?>
            <span><?= $item['cat_title'] ?></span>
        </div>
        <div class="sr-page__gallery__item_title">
            <a href="<?= $item['link'] ?>"><?= $item['title'] ?></a>
        </div>
        <?php if ($item['price_on']): ?>
        <p class="sr-page__gallery__item_price">
            <strong><?= $item['price'] ?></strong>
            <small><?= $item['price_mod'] ?></small>
        </p>
        <?php endif; ?>
        <div class="sr-page__bottom-info">
            <?php if ( ! empty($address)): ?>
            <p class="sr-page__item__address">
                <small><?= join(', ', $address) ?></small>
            </p>
            <?php endif; ?>
            <?php if( ! empty($item['descr_list'])): ?>
            <div class="sr-page__item__desc extra__content">
                <?= $item['descr_list'] ?>
            </div>
            <?php endif; ?>
            <div class="sr-page__item__date extra__content">
                <?php if ($item['publicated_up']): ?>
                    <span class="grey">
                        <i class="fa fa-refresh"></i>
                        <span class="ajax-in j-tooltip" data-toggle="tooltip" data-container="body" data-placement="bottom" data-html="true" data-original-title="<div class='text-left'><?= _te('search', 'Обновлено: [date]', ['date'=>$item['publicated_last']]); ?></div> <div class='text-left'><?= _te('search', 'Размещено: [date]', ['date'=>$item['publicated']]); ?></div>"><?= $item['publicated_last'] ?></span>
                    </span>
                <?php else: ?>
                    <span class="grey"><?= $item['publicated'] ?></span>
                <?php endif; ?>
            </div>
        </div>
    </div>
  </div>
</div>