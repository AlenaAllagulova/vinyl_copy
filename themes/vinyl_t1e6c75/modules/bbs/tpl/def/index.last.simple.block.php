<?php
/**
 * Блок объявлений на главной
 * @var $this BBS
 * @var $title string заголовок блока
 * @var $items array данные об объявлениях
 * @var $type string тип объявлений: 'last', 'premium'
 */
$lng_fav_in = _te('bbs', 'Добавить в избранное');
$lng_fav_out = _te('bbs', 'Удалить из избранного');
$lng_quick = _t('bbs', 'срочно');
?>
<div class="" id="j-bbs-index-<?= $type ?>-block">
    <div class="index__heading__title"><?= $title ?></div>
  <div>
    <?php foreach($items as &$v) {
      // Gallery Item Template
      echo View::template('search.item.gallery', array('item'=>&$v), 'bbs');
    } unset ($v); ?>
  </div>
</div>
