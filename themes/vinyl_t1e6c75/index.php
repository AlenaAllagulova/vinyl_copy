<?php

class Theme_Vinyl_t1e6c75 extends Theme
{
    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'extension_id'  => 't1e6c75055c5f03ac3f1f820443e0f3433c0bea5',
            'theme_title'   => 'Тема "Vinyl"',
            'theme_version' => '1.0.0',
            'theme_parent'  => 'nelson_t001e26',
        ));

        /**
         * Настройки заполняемые в админ. панели
         */
        $this->configSettings(array(
            //
        ));
        
        $this->cssEdit(array(
            static::CSS_FILE_CUSTOM => ['path' => $this->path('/static/css/custom.css', false), 'save' => true],
        ));
    }

    /**
     * Запуск темы
     */
    protected function start()
    {
        //
    }
}