// Filter Position in LocalStorage
$('#j-f-desktop').collapse().on('hidden.bs.collapse', function () {
  $('.l-filter-form-toggle').addClass('collapsed');
  localStorage[this.id] = 'true';
}).on('shown.bs.collapse', function () {
  $('.l-filter-form-toggle').removeClass('collapsed');
  localStorage.removeItem(this.id);
}).each(function () {
  if (localStorage[this.id] === 'true') {
    $(this).removeClass('in');
    $('.l-filter-form-toggle').addClass('collapsed');
  }
});

$(function () {

  // Mega Dropdown
  $(document).on('click', '.mega-dropdown', function (e) {
    e.stopPropagation();
  });

  // Sticky Footer
  $(window).on('load resize', function () {
    var footerHeight = $('.j-footer').height();
    $('.page-wrap').css({"margin-bottom": -footerHeight - 1});
    $('.page-wrap-after').css({"height": footerHeight});
  });

  // Tooltips and popovers
  $('.has-tooltip').tooltip();
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();

  // Load last items to main page
  if (Boolean($('.j-last_items-load').length)) {
    $('.j-last_items-load').on('click', function() {
      var self = $(this);
      var data = self.data();
      var $box = $(data.target);

      if ( ! Boolean($box.length) || self.attr('disabled') == 'disabled') {
        return false;
      }

      self.attr('disabled', 'disabled');

      bff.ajax(
        bff.ajaxURL('vinylhandler','get-bbs_items'),
        data,
        function(resp, errors) {
          if(resp && resp.success) {
            self.data('offset', resp.data.offset);
            var offset = resp.data.offset;
            $box.append(resp.data.content);
            self.removeAttr('disabled');
            if ( ! Boolean(offset)) {
              self.hide();
              return false;
            }
          } else {
              app.alert.error(errors);
          }
      });
    });
  }

    $('#j-f-query').on('change keyup input', $.debounce(function(){
        var btnClear = $('.j-clear-input-search');
        if( btnClear.length > 0) {
            if($('#j-f-query').val().length > 0) {
                btnClear.show();
                btnClear.on('click', function (e) {
                    e.preventDefault();
                    $('#j-f-query').val('');
                    $('input[name="mq"]').val('');
                    btnClear.hide();
                    $('#j-f-query').focus();
                })
            }else {
                btnClear.hide();
            }
        }

    }, 100));

});