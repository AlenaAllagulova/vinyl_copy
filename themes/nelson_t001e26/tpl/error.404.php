<?php
/**
 * Текст ошибки 404 (страница не найдена)
 */
?>
<div class="l-content">
  <div class="container">
<div class="around-page">
      <div class="super-login-page">
            <div class="blue-line"></div>
      <div class="insidepadding">
    <div class="l-headerShort-heading">
      <h1 class="l-headerShort-heading-title j-shortpage-title"><?= _t('error', 'Страница не найдена') ?></h1>
    </div>
    <p>
      <?= _t('error', 'Такой страницы не существует.') ?>
    </p>
    <p><?= _t('error', 'Перейти на <a [home]>главную страницу</a>.', array('home'=>'href="'.bff::urlBase().'"')) ?></p>
    <p><?= _t('error', 'Если вы уверены в том, что эта странца здесь должна быть, то <a [contact]>напишите нам</a>, пожалуйста.', array('contact'=>'href="'.Contacts::url('form').'"')) ?></p>
    <form action="<?= BBS::url('items.search') ?>" class="form-inline">
      <div class="form-group">
        <input type="text" class="form-control" name="q" maxlength="80">
      </div>
      <button type="submit" class="btn btn-default"><?= _t('error', 'Найти') ?></button>
    </form>
          </div>
                  <div class="blue-line-bottom"></div>
      </div>
      </div>
  </div>
</div>