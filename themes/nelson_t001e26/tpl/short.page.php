<?php
/**
 * Содержимое страниц сокращенного вида
 * @var $title string заголовок
 * @var $content string содержимое (HTML)
 */
?>
<div class="around-page">
      <div class="super-login-page">
            <div class="blue-line"></div>
      <div class="insidepadding">
<div class="l-headerShort-heading">
  <h1 class="l-headerShort-heading-title"><?= $title ?></h1>
</div>
<?= $content ?>
      </div>
                  <div class="blue-line-bottom"></div>
      </div>
      </div>