<?php
/**
 * Layout: основной каркас
 * @var $centerblock string содержимое (HTML)
 */
?>
<!DOCTYPE html>
<html xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml" class="no-js">
<head>
<? // todo claxion пока не профиксят Access-Control-Allow-Origin  ?>
<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" media="all" type="text/css" />-->
<? // todo claxion пока не профиксят Access-Control-Allow-Origin  ?>
<?php View::blockStart('head'); ?>
    <?= SEO::i()->metaRender(array('content-type'=>true,'csrf-token'=>true)) ?>
<?= View::template('css'); ?>

<?php View::blockEnd(); ?>
<meta name="format-detection" content="telephone=no">
</head>
<body <?php if(bff::theme()->config('header.fixed')) { ?> class="fixed-header"<?php } ?>>
<div class="responsivepage">
<?php View::blockStart('body'); ?>
  <?= View::template('alert'); ?>
  <div class="page-wrap">
    <!-- Top Banner -->
    <?php if( DEVICE_DESKTOP_OR_TABLET && ($bannerTop = Banners::view('site_top')) ) { ?>
    <div class="l-banner-top">
      <?= $bannerTop; ?>
    </div>
    <?php } ?>
    <!-- Header -->
    <?= View::template('header'); ?>
    <!-- Filter -->
    <?= View::template('filter'); ?>
    <!-- Content -->
    <?= $centerblock; ?>
    <div class="page-wrap-after"></div>
  </div>
  <!-- Footer -->
  <?= View::template('footer'); ?>

  <?php if(DEVICE_DESKTOP_OR_TABLET) { ?> 
    <!-- Back to top -->
    <a href="#" class="scrolltop" id="j-scrolltop" style="display: none;"><i class="fa fa-arrow-up"></i></a>
  <?php } ?>

  <!-- Scripts -->
  <?= View::template('js'); ?>
  <?= js::renderInline(js::POS_FOOT); ?>
<?php View::blockEnd(); ?>
</div>
</body>
</html>