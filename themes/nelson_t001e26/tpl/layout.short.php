<?php
/**
 * Layout: для страниц сокращенного вида
 * @var $centerblock string содержимое (HTML)
 */
?>
<!DOCTYPE html>
<html xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml" class="no-js">
<head>
<?php View::blockStart('head'); ?>
<?= SEO::i()->metaRender(array('content-type'=>true,'csrf-token'=>true)) ?>
<?= View::template('css'); ?>
<?php View::blockEnd(); ?>
</head>
<body>
<div class="responsivepage">
<?php View::blockStart('body'); ?>
  <?= View::template('alert'); ?>
  <div class="page-wrap login-page">
    <!-- Header -->
    <?= View::template('header.short'); ?>
    <!-- BEGIN main content -->
    <div class="l-content">
      <div class="container login-page">
        <?= $centerblock; ?>
      </div>
    </div>
    <div class="page-wrap-after"></div>
  </div>
  <!-- Scripts -->
  <?= View::template('js'); ?>
  <?= js::renderInline(js::POS_FOOT); ?>
<?php View::blockEnd(); ?>
</div>
</body>
</html>