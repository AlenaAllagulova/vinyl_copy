<?php
/**
 * Восстановление пароля: Шаг 1
 * @var $this Users
 * @var $social integer инициировано ли восстановление на этапе авторизации через соц. сеть
 */
?>

      <div class="super-login-page-title pass">
      <h3>Восстановление пароля</h3>
      </div>

      <div style="clear: both"></div>

        <div class="formaregi">
          <form action="" id="j-u-forgot-start-form-<?= bff::DEVICE_DESKTOP ?>" class="form-horizontal">
            <input type="hidden" name="social" value="<?= $social ?>" />
            <div class="">
              <div class="">
                <input class="form-control j-required reg" type="email" name="email" id="j-u-forgot-start-desktop-email" placeholder="<?= _te('users', 'Введите ваш email') ?>" maxlength="100" autocorrect="off" autocapitalize="off" />
              </div>
            </div>
            <div class="borow">
              <div class="reg-button">
                <button type="submit" class="btn btn-success j-submit big"><?= _t('users', 'Отправить') ?></button>
              </div>
            </div>
          </form>
      <div class="or-line nobold">
          <a href="<?= Users::url('login') ?>"><?= _t('users', 'Я вспомнил(а) свой пароль') ?></a>
      </div>

      </div>





<script type="text/javascript">
  <? js::start(); ?>
  $(function(){
    jUserAuth.forgotStart(<?= func::php2js(array(
      'lang' => array(
        'email' => _t('users', 'E-mail адрес указан некорректно'),
        'success' => _t('users', 'На ваш электронный ящик были высланы инструкции по смене пароля.'),
        ),
      )) ?>);
  });
  <? js::stop(); ?>
</script>