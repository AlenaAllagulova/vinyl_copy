<?php
  /**
   * Профиль пользователя (layout)
   * @var $this Users
   * @var $user array данные пользователя
   * @var $is_owner boolean профиль просматривает его владелец
   */
  ?>

<div class="l-content" id="j-shops-v-container">
  <div class="container container_sm">
    <div class="l-pageHeading">
      <h1 class="l-pageHeading-title"><?= (!empty($titleh1) ? $titleh1 : _t('bbs', 'Объявления пользователя')) ?></h1>
    </div>
    <div class="container-mobile">
      <div class="l-content-box">
        <div class="l-content-box-cols">
          <div class="l-content-box-left">
              <? if (DEVICE_PHONE): ?>
                  <div class="ad-author mrgt25 mrgb25 mrgl0 mrgr0">
                      <div class="ad-author-in ad-author-user">
                          <a href="<?= $user['profile_link'] ?>" class="ad-author-user-avatar">
                              <img src="<?= $user['avatar'] ?>" alt="" />
                          </a>
                          <div class="ad-author-user-info">
                              <div class="ad-author-user-name"><?= $user['name'] ?></div>
                              <!-- plugin_user_online_do_block -->
                              <? if($user['region_id']){ ?>
                                  <div class="ad-author-user-subtext"><?= $user['region_title'] ?></div>
                              <? } ?>
                              <div class="ad-author-user-subtext"><?= _t('users', 'на сайте с [date]', array('date'=>tpl::date_format2($user['created']))) ?></div>
                          </div>
                      </div>
                      <? if(!empty($user['phones']) || !empty($user['contacts'])) { ?>
                          <div class="ad-author-in ad-author-contact">
                              <div class="ad-author-contact-row pd10" style="display: block!important;">
                                  <div class="ad-author-contact-row-label">
                                      <?= _t('users', 'Контакты') ?>:
                                  </div>
                                  <div class="ad-author-contact-row-content">
                                      <a href="#" class="link-ajax j-user-profile-c-toggler"><span><?= _t('users', 'показать контакты') ?></span></a>
                                  </div>
                              </div>

                              <? if( ! empty($user['phones']) ) { ?>
                                  <div class="ad-author-contact-row pd10" style="display: block!important;">
                                      <div class="ad-author-contact-row-label">
                                          <?= _t('users', 'Тел.:') ?>
                                      </div>
                                      <div class="ad-author-contact-row-content j-user-profile-c-phones">
                                          <? foreach($user['phones'] as $v) { ?><div><?= $v['m'] ?></div><? } ?>
                                      </div>
                                  </div>
                              <? } # phones ?>

                              <? if (!empty($user['contacts'])): ?>
                                  <?php foreach (Users::contactsFields($user['contacts']) as $contact): ?>
                                      <div class="ad-author-contact-row pd10" style="display: block!important;">
                                          <div class="ad-author-contact-row-label">
                                              <?= $contact['title'] ?>:
                                          </div>
                                          <div class="ad-author-contact-row-content j-user-profile-c-<?= $contact['key'] ?>">
                                              <?= tpl::contactMask($contact['value']) ?>
                                          </div>
                                      </div>
                                  <?php endforeach; ?>
                              <? endif; # contacts ?>

                          </div><!-- /.ad-author-contact -->
                      <? } ?>
                      <? if($is_owner) { ?>
                          <div class="ad-author-in">
                              <a href="<?= Users::url('my.settings', array('t'=>'contacts')) ?>" class="btn btn-default btn-block btn-sm"><i class="fa fa-edit"></i> <?= _t('users', 'Редактировать') ?></a>
                          </div>
                      <? } ?>
                  </div>
              <? endif; ?>

            <div class="l-content-box-in">
              <?= $content ?>
            </div>
          </div>
          <?php if (DEVICE_DESKTOP_OR_TABLET) { ?>
          <div class="l-content-box-sidebar">
            <?= // Owner block
              View::template('profile.owner', $aData, 'users');
            ?>
            <?php if ($bannerRight = Banners::view('users_profile_right')) { ?>
            <div class="l-content-box-in">
              <?= $bannerRight ?>
            </div>
            <?php } ?>
          <?php } ?>
          </div>
        </div>
      </div>
  </div>
</div>

<script type="text/javascript">
<? js::start(); ?>
$(function(){
  var _process = false;
  $('.j-user-profile-c-toggler').on('click touchstart', function(e){
    nothing(e); if(_process) return;
    var $link = $(this);
    bff.ajax(bff.ajaxURL('users','user-contacts'), {hash:app.csrf_token, ex:'<?= $user['user_id_ex'] ?>-<?= $user['user_id'] ?>'},
      function(data, errors) {
        if(data && data.success) {
            if (data.hasOwnProperty('phones')) {
                $('.j-user-profile-c-phones').html(data['phones']);
            }
            if (data.hasOwnProperty('contacts')) {
                for(var c in data.contacts) {
                    if (data.contacts.hasOwnProperty(c)) {
                        $('.j-user-profile-c-' + c).html(data.contacts[c]);
                    }
                }
            }
            $link.remove();
        } else {
          app.alert.error(errors);
        }
      }, function(p){ _process = p; }
      );
  });
});
<? js::stop(); ?>
</script>