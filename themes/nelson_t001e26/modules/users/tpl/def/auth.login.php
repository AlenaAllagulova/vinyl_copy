<?php
/**
 * Авторизация
 * @var $this Users
 * @var $back string URL страницы возврата (после успешной авторизации)
 * @var $providers array провайдеры авторизации через соц. сети
 */
?>

      <div class="super-login-page-title">
      <h3>Войти</h3>
      </div>

      <div class="need-help">
          <a href="<?= Users::url('register') ?>"><?= _t('users', 'Регистрация') ?></a>
      </div>
      <div style="clear: both"></div>

        <div class="formaregi">
          <form class="form-horizontal-new" id="j-u-login-form" action="">
            <input type="hidden" name="back" value="<?= HTML::escape($back) ?>" />
            <div class="form-group">
  <!--             <label class="col-md-3 col-sm-4 control-label" for="j-u-login-email"><?= _t('users', 'Электронная почта') ?><span class="required-mark">*</span></label>  -->
              <div class="">
                <input type="email" name="email" class="form-control reg" id="j-u-login-email" placeholder="<?= _te('users', 'Введите ваш email') ?>" maxlength="100" autocorrect="off" autocapitalize="off" />
              </div>
            </div>
            <div class="form-group">
 <!--             <label class="col-md-3 col-sm-4 control-label" for="j-u-login-pass"><?= _t('users', 'Пароль') ?><span class="required-mark">*</span></label>  -->
              <div class="">
                <input type="password" name="pass" class="form-control reg" id="j-u-login-pass" placeholder="<?= _te('users', 'Введите ваш пароль') ?>" maxlength="100" />
              </div>
            </div>
            <? if(Users::loginRemember()) { ?>
            <div class="">
              <div class="left-reg">
                <div class="checkbox reg">
                  <label>
                    <input type="checkbox" name="remember"/> <?= _t('users', 'Запомнить меня'); ?>
                  </label>
                </div>
              </div>
              <div class="help-right">
                <a href="<?= Users::url('forgot') ?>"><?= _t('users', 'Напомнить пароль') ?></a>
              </div>
            </div>
                  <div style="clear: both"></div>
            <? } ?>
            <div class="borow">
              <div class="reg-button">
                <button type="submit" class="btn btn-success j-submit big"><?= _t('users', 'Войти на сайт') ?></button>
              </div>
            </div>
          </form>


                    <div class="or-line">или</div>
        <?php if ($providers) { ?>
        <div class="facebook-login">
          <? foreach($providers as $v) { ?><a href="#" class="btn btn-sm btn-social btn-<?= $v['class'] ?> j-u-login-social-btn" data="{provider:'<?= $v['key'] ?>',w:<?= $v['w'] ?>,h:<?= $v['h'] ?>}"><?= $v['title'] ?></a><? } ?>
        </div>
        <?php } ?>
      </div>



<script type="text/javascript">
  <? js::start(); ?>
  $(function(){
    jUserAuth.login(<?= func::php2js(array(
      'login_social_url' => Users::url('login.social'),
      'login_social_return' => $back,
      'lang' => array(
        'email' => _t('users', 'E-mail адрес указан некорректно'),
        'pass' => _t('users', 'Укажите пароль'),
        ),
      )) ?>);
  });
  <? js::stop(); ?>
</script>