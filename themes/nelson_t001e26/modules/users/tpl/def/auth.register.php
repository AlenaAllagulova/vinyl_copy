<?php
/**
 * Регистрация - форма регистрации
 * @var $this Users
 * @var $back string URL страницы возврата (после успешной регистрации)
 * @var $phone_on boolean включена регистрация с вводом номера телефона
 * @var $pass_confirm_on boolean включено подтверждение пароля
 * @var $captcha_on boolean включена капча
 * @var $providers array провайдеры авторизации через соц. сети
 */
?>


      <div class="super-login-page-title">
      <h3>Регистрация</h3>
      </div>

      <div class="need-help">
          <a href="<?= Users::url('login') ?>"><?= _t('users', 'Войти') ?></a>
      </div>
      <div style="clear: both"></div>

        <div class="formaregi">
          <form class="form-horizontal-new" id="j-u-register-form" action="">
            <input type="hidden" name="back" value="<?= HTML::escape($back) ?>" />
            <? if($phone_on) { ?>
            <div class="form-group">
              <div class="">
                <?= $this->registerPhoneInput(array('id'=>'j-u-register-phone','name'=>'phone')) ?>
              </div>
            </div>
            <? } ?>
            <div class="form-group">
              <div class="">
                <input type="email" name="email" class="form-control j-required reg" id="j-u-register-email" autocomplete="off" placeholder="<?= _te('users', 'Введите ваш email') ?>" maxlength="100" autocorrect="off" autocapitalize="off" />
              </div>
            </div>
            <div class="form-group">
              <div class="">
                <input type="password" name="pass" class="form-control j-required reg" id="j-u-register-pass" autocomplete="off" placeholder="<?= _te('users', 'Придумайте пароль') ?>" maxlength="100" />
              </div>
            </div>
            <? if($pass_confirm_on) { ?>
            <div class="form-group">
              <div class="">
                <input type="password" name="pass2" class="form-control j-required reg" id="j-u-register-pass2" autocomplete="off" placeholder="<?= _te('users', 'Введите пароль ещё раз') ?>" maxlength="100" />
              </div>
            </div>
            <? } ?>
            <div class="form-group agr">
            <div class="agreement">
              <div class="checkbox reg">
                <label>
                  <input type="checkbox" name="agreement" id="j-u-register-agreement" autocomplete="off" /> <?= _t('users', 'Я соглашаюсь с <a href="[link_agreement]" target="_blank">условиями использования</a>, передачей и обработкой моих данных.', array('link_agreement'=>Users::url('agreement'))) ?><span class="required-mark">*</span>
                </label>
              </div>
            </div>
            </div>
            <? if($captcha_on) { ?>
              <?php if (Site::captchaCustom('users-auth-register')) { ?>
                <div class="form-group">
                  <?php bff::hook('captcha.custom.view', 'users-auth-register', __FILE__); ?>
                </div>
              <?php } else { ?>
                <div class="form-group">
                  <label class="col-md-3 col-sm-4 control-label" for="j-u-register-captcha"><?= _t('users', 'Результат с картинки') ?><span class="required-mark">*</span></label>
                  <div class="">
                    <div class="row">
                      <div class="col-xs-6">
                        <input type="text" name="captcha" id="j-u-register-captcha" autocomplete="off" class="form-control j-required" value="" pattern="[0-9]*" />
                      </div>
                      <div class="col-xs-6">
                        <img src="<?= tpl::captchaURL() ?>" class="j-captcha" onclick="$(this).attr('src', '<?= tpl::captchaURL() ?>&rnd='+Math.random())" />
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>
            <? } ?>

            <div class="borow">
              <div class="reg-button">
                <button type="submit" class="btn btn-success j-submit big"><?= _t('users', 'Зарегистрироваться') ?></button>
              </div>
            </div>
          </form>

        <div class="or-line">или</div>

        <?php if ($providers) { ?>
        <div class="facebook-login">
          <? foreach($providers as $v) { ?><a href="#" class="btn btn-sm btn-social btn-<?= $v['class'] ?> j-u-login-social-btn" data="{provider:'<?= $v['key'] ?>',w:<?= $v['w'] ?>,h:<?= $v['h'] ?>}"><?= $v['title'] ?></a><? } ?>
        </div>
        <?php } ?>
      </div>




<script type="text/javascript">
  <? js::start(); ?>
  $(function(){
    jUserAuth.register(<?= func::php2js(array(
      'phone' => $phone_on,
      'captcha' => !empty($captcha_on) && ! Site::captchaCustom('users-auth-register'),
      'pass_confirm' => !empty($pass_confirm_on),
      'login_social_url' => Users::url('login.social'),
      'login_social_return' => $back,
      'lang' => array(
        'email' => _t('users', 'E-mail адрес указан некорректно'),
        'pass' => _t('users', 'Укажите пароль'),
        'pass2' => _t('users', 'Пароли должны совпадать'),
        'captcha' => _t('users', 'Введите результат с картинки'),
        'agreement' => _t('users', 'Пожалуйста подтвердите, что Вы согласны с пользовательским соглашением'),
      ),
    )) ?>);
  });
  <? js::stop(); ?>
</script>