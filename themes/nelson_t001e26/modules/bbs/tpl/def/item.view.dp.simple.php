<?php use bff\db\Dynprops;

/**
 * Просмотр объявления: блок дин. свойств
 * @var $this bff\db\Dynprops
 * @var $dynprops array for simple view
 */

$dynprops = json_encode($dynprops);
echo $dynprops;
