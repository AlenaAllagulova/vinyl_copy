<?php
/**
 * Блок премиум объявлений
 * @var $this BBS
 * @var $items array объявления
 */
?>
<? if(DEVICE_DESKTOP_OR_TABLET): ?>
<?php foreach($items as $v): ?>
    <li>
        <a href="<?= $v['link'] ?>">
            <?php if( ! empty($v['img_m'])): ?><div class="sr-vip-content-img"><img src="<?= $v['img_m'] ?>" alt="<?= $v['title'] ?>"></div><?php endif; ?>
            <div class="sr-vip-content-title"><?= $v['title'] ?></div>
            <div class="sr-vip-content-price">
                <?php if($v['price_on']) { ?>
                    <span class="vip-price"><?= $v['price'] ?></span>
                    <small><?= $v['price_mod'] ?></small>
                <?php } ?>
            </div>
        </a>
    </li>
<? endforeach; ?>
<? else: ?>
    <?php foreach ($items as $item): ?>
        <div class="sr-gallery-item ">
            <div class="sr-gallery-item-img">
                <div class="super-puper-gallery-item-img">
                    <div class="super-gallery-item-img" style="background-image: url(<?= $item['img_m'] ?>)">
                    </div>
                    <div class="noblur-img">
                        <a href="<?= $item['link'] ?>" title="<?= $item['title'] ?>" class="sr-glItem-img<?php if ($item['imgs'] > 1) { ?> sr-glItem-img_multiple<?php } ?>">
                            <img src="<?= $item['img_m'] ?>" alt="<?= $item['title'] ?>" />
                        </a>
                    </div>
                </div>
            </div>
            <div class="sr-gallery-item-content">
                <div class="sr-gallery-item-heading">
                    <?php if ($item['svc_quick']) { ?><span class="label label-urgent"><?= _t('bbs', 'раритет') ?></span><?php } ?>
                    <div class="sr-gallery-item-heading-title"><a href="<?= $item['link'] ?>"><?= $item['title'] ?></a></div>
                </div>

                <div class="c-price sr-gallery-item-price">
                    <?php if ($item['price_on']) { ?>
                        <div class="pricik">
                            <?= $item['price'] ?>
                        </div>
                        <!--      <div class="c-price-subik">
         <span class="c-price-sub"><?= $item['price_mod'] ?></span>
      </div> -->
                    <?php } ?>
                </div>
                <div class="sr-glItem-subtext">
                    <span class="sr-glItem-subtext-i"><?= $item['cat_title'] ?></span>
                    <span class="super-sr-list-item-region"><?php if ( ! empty($item['city_title'])): ?><i class="fa fa-map-marker"></i> <?= $item['city_title'] ?><?= ! empty($item['district_title']) ? ', '.$item['district_title'] : ''?><?php endif; ?></span>
                </div>
            </div>
        </div>
    <? endforeach; ?>
<? endif; ?>