<?php use bff\db\Dynprops;

/**
 * Форма объявления: добавление / редактирование - дин. свойства
 * @var $this Dynprops
 * @var $dynprops array основные дин. свойства
 * @var $children array вложенные дин. свойства
 */

$itemData = Vinylhandler::model()->getLastUserItem();
$lastItemDynProps = json_decode(BBSBase_::i()->dpView($itemData['cat_id'], $itemData, 'item.view.dp.simple'), true);

$selectedCategoriesData = Vinylhandler::model()->getSelectedCategoryData($extra['selected_category_id']);

$extra['existsBaseGroupDP'] = false;
$extra['existsPublisherGroupDP'] = false;
# Добавляем в список дин свойств год

if (!!$selectedCategoriesData['year_enabled']) {
    $dynprops[] = [
        'name'  => 'year',
        'value' => !empty($extra['edit']) ? $extra['year'] : '',
        'type'  => Dynprops::typeInputText,
        'req'   => false,
        'title' => $selectedCategoriesData['year_title'],
        'num_first' => true,
        'group_id'  => Vinylhandler::PUBLISHER_TYPES[$selectedCategoriesData['year_show_in']],
    ];
}

$drawControl = function ($title, $value, $required, array $class = array(), $extra = array()) {
    $doNotPrintColMdColSm = !empty($extra['without_style']);
    $data = [];
    if ($required) $class[] = 'j-required';
    if (isset($extra['data'])) {
        foreach ($extra['data'] as $k => $val) {
            $data[] = 'data-' . $k . '="' . $val . '"';
        }
    }
  ?>
  <div class="form-group j-control-group<?= (!empty($class) ? ' ' . join(' ', $class) : '') ?>"
      <?= (!empty($data) ? ' ' . join(' ', $data) : '') ?> >
    <label class="<?=$doNotPrintColMdColSm ? '' : 'col-sm-3'?> control-label"><?= $title ?><? if ($required) { ?><span class="required-mark">*</span><? } ?>
    </label>
    <div class="<?=$doNotPrintColMdColSm ? '' : 'col-md-3 col-sm-6'?>">
      <?= $value ?>
    </div>
  </div>
  <?
};

# ---------------------------------------------------------------------------------------
# Дин. свойства:
$aExtraSettings = $this->extraSettings();

/**
 * Отрисовка дин. свойств
 * @param bff\db\Dynprops $self
 * @param boolean $numFirst дин. св-ва помеченные вне очереди (num_first = 1)
 */
$ownerColumn = $this->ownerColumn;
$drawDynprops = function ($self, $numFirst = false, $numBaseGroup = false, $existsGroupDP = false) use (&$dynprops, &$aExtraSettings, $ownerColumn, $drawControl, &$children, $lastItemDynProps, &$extra) {
  $prefix = 'd';
  foreach ($dynprops as $dpKey => $d) {
    if (!empty($extra['only_base_group']) && $d['group_id'] !== Vinylhandler::DYN_PROP_BASE_GROUP) {
        continue;
    }
      # Наличие дин. св-в групп "simple" или "shop", "simple-shop" соответственно выбранному типу ОБ
      if( in_array($d['group_id'], Vinylhandler::PUBLISHER_TYPES) && ($d['group_id'] == $extra['publisher_type_from_shop'] || $d['group_id'] == Vinylhandler::DYN_PROP_GROUP_SIMPLE_SHOP)){
          $extra['existsPublisherGroupDP'] = true;
      }
    if (($numFirst && isset($d['num_first']) && !$d['num_first']) ||
      (!$numFirst && $d['num_first'])
    ) continue;

    # Наличие дин. св-в базовой категории группы "0" для выбранного типа ОБ
    if (isset($extra['publisher_type_from_shop']) && !empty($extra['publisher_type_from_shop'])
        && Vinylhandler::isUseBaseDynPropInItemType($extra['publisher_type_from_shop'])
        && $d['group_id'] === Vinylhandler::DYN_PROP_BASE_GROUP) {
        $extra['existsBaseGroupDP'] = true;
    }

    if ($existsGroupDP){ continue; }

    if ($numBaseGroup && ($d['group_id'] !== Vinylhandler::DYN_PROP_BASE_GROUP) ||
        !$numBaseGroup && ($d['group_id'] === Vinylhandler::DYN_PROP_BASE_GROUP)
    ) continue;

    if (!empty($d['id'])) {
        $ID = $d['id'];
        $ownerID = $d[$ownerColumn];
        $name = $prefix . '[' . $ownerID . ']' . '[' . $ID . ']';
        $nameChild = $prefix . '[' . $ownerID . ']';
    } else {
        $name = $d['name'];
    }
    
    $html = '';
    $class = array('j-dp');

    # метки доп. настроек
    foreach ($aExtraSettings as $k => $v) {
      if ($v['input'] == 'checkbox' && !empty($d[$k])) {
        $class[] = 'j-dp-ex-' . $k;
      }
    }
    if( isset($extra['publisher_type_from_shop']) && !empty($extra['publisher_type_from_shop']) ) {
        # скрываем дин.св-ва базовой группы "0" в зависимости настройки демонстрации от типа объявления
        if(!Vinylhandler::isUseBaseDynPropInItemType($extra['publisher_type_from_shop'])
           && $d['group_id'] === Vinylhandler::DYN_PROP_BASE_GROUP) {
              $class[] = 'hide';
        }
        # скрываем дин.св-ва не входящие: 1) базовую группу "0", 2)группы shop или simple в зависимости от типа объявления,
        # 3) группа 'simple-shop' - принадлежность ко всем типам публикации
        if($d['group_id'] !== Vinylhandler::DYN_PROP_BASE_GROUP
           && $d['group_id'] !== $extra['publisher_type_from_shop'] && $d['group_id'] != Vinylhandler::DYN_PROP_GROUP_SIMPLE_SHOP){
            $class[] = 'hide';
        }
    }

    if($d['group_id'] === Vinylhandler::DYN_PROP_BASE_GROUP){
      $extra['data'] = [ 'group_id' => Vinylhandler::DYN_PROP_BASE_GROUP ];
    } else{
        $extra['data'] = [ 'group_id' => ''];
    }

    if( !empty($d['group_id']) && in_array($d['group_id'], Vinylhandler::PUBLISHER_TYPES)){
      $extra['data'] = [ 'group_id' => $d['group_id'] ];
    }

    switch ($d['type']) {
      # Группа св-в с единичным выбором
      case Dynprops::typeRadioGroup: {
        $value = (isset($d['value']) ? $d['value'] : $d['default_value']);
        if (!empty($d['group_one_row'])) {
          $html = '';
          foreach ($d['multi'] as $v) {
            if (!$v['value']) continue;
            $html .= '<div class="radio"><label><input type="radio" name="' . $name . '"
                                ' . ($v['value'] == $value ? ' checked="checked"' : '') . ' value="' . $v['value'] . '" data-num="' . $v['num'] . '" />' . $v['name'] . '</label></div>';
          }
        } else {
          $html = HTML::renderList($d['multi'], $value, function ($k, $i, $values) use ($name) {
            $v = &$i['value'];
            if (!$v) return '';
            return '<li><div class="radio"><label><input type="radio" name="' . $name . '"
                                ' . ($v == $values ? ' checked="checked"' : '') . ' value="' . $v . '" data-num="' . $i['num'] . '" />' . $i['name'] . '</label></div></li>';
          },
            array(2 => 4, 3 => 15),
            array('class' => 'unstyled span' . (sizeof($d['multi']) > 15 ? 4 : 6))
          );
        }
        if (!empty($d['description'])) {
          $html .= '<input type="hidden" /><span class="help-block">' . $d['description'] . '</span>';
        }
      }
        break;

      # Группа св-в с множественным выбором
      case Dynprops::typeCheckboxGroup: {
        $value = (isset($d['value']) && $d['value'] ? explode(';', $d['value']) : explode(';', $d['default_value']));

        $lastItemValue = [];
        if ($d['group_id'] === Vinylhandler::DYN_PROP_BASE_GROUP && empty($extra['edit']) && (!isset($d['value']) || empty($d['value']))  ) {
            $value = (isset($lastItemDynProps[$d['id']]['value']) && $lastItemDynProps[$d['id']]['value'])
                ? explode(';', $lastItemDynProps[$d['id']]['value'])
                : [];
            $lastItemValue = $value;
        }
        if (!empty($d['group_one_row'])) {
          $html = '';
          foreach ($d['multi'] as $v) {
            $html .= '<div class="checkbox"><label><input type="checkbox" name="' . $name . '[]"
                                ' . (in_array($v['value'], $value) || in_array($v['value'], $lastItemValue) ? ' checked="checked"' : '') . ' value="' . $v['value'] . '" data-num="' . $v['num'] . '" />' . $v['name'] . '</label></div>';
          }
        } else {
          $html = HTML::renderList($d['multi'], $value, function ($k, $i, $values) use ($name) {
            $v = &$i['value'];
            return '<li><div class="checkbox"><label class="checkbox"><input type="checkbox" name="' . $name . '[]"
                                ' . (in_array($v, $values) ? ' checked="checked"' : '') . ' value="' . $v . '" data-num="' . $i['num'] . '" />' . $i['name'] . '</label></div></li>';
          },
            array(2 => 4, 3 => 15),
            array('class' => 'unstyled span' . (sizeof($d['multi']) > 15 ? 4 : 6))
          );
        }
        if (!empty($d['description'])) {
          $html .= '<input type="hidden" /><span class="help-block">' . $d['description'] . '</span>';
        }
      }
        break;

      # Выбор Да/Нет
      case Dynprops::typeRadioYesNo: {
        $value = (isset($d['value']) ? $d['value'] : $d['default_value']);
        $html = '<div class="radio-inline"><label><input type="radio" name="' . $name . '" value="2" ' . ($value == 2 ? 'checked="checked"' : '') . ' />' . $self->langText['yes'] . '</label></div>
                         <div class="radio-inline"><label><input type="radio" name="' . $name . '" value="1" ' . ($value == 1 ? 'checked="checked"' : '') . ' />' . $self->langText['no'] . '</label></div>';
        if (!empty($d['description'])) {
          $html .= '<input type="hidden" /><span class="help-block">' . $d['description'] . '</span>';
        }
      }
        break;

      # Флаг
      case Dynprops::typeCheckbox: {
        $value = (isset($d['value']) ? $d['value'] : $d['default_value']);
        $html = '<div class="checkbox"><label><input type="hidden" name="' . $name . '" value="0" /><input type="checkbox" name="' . $name . '" value="1" ' . ($value ? 'checked="checked"' : '') . ' />' . $self->langText['yes'] . '</label></div>';
        if (!empty($d['description'])) {
          $html .= '<input type="hidden" /><span class="help-block">' . $d['description'] . '</span>';
        }
      }
        break;

      # Выпадающий список
      case Dynprops::typeSelect: {
        $value = (isset($d['value']) ? $d['value'] : $d['default_value']);
        if ($d['parent']) {
          $html = '<select class="form-control" name="' . $name . '" onchange="jForm.dpSelect(' . $d['id'] . ', this.value, \'' . $nameChild . '\');">';
          $html .= HTML::selectOptions($d['multi'], $value, false, 'value', 'name');
          $html .= '</select>';
          if (!empty($d['description'])) {
            $html .= '<span class="help-block">' . $d['description'] . '</span>';
          }
          $drawControl($d['title'], $html, $d['req'], $class);

          $html = '<span class="j-dp-child-' . $ID . '">';
          if (!empty($value) && isset($children[$ID])) {
            $html .= $self->formChild($children[$ID], array('name' => $nameChild, 'class' => ($d['req'] ? ' j-required' : '')));
          }
          $html .= '</span>';
          if (empty($value)) {
            $class[] = 'hide';
            $class[] = 'j-dp-child-hidden';
          }
          $drawControl($d['child_title'], $html, $d['req'], $class);

          continue 2;
        } else {
          $html = '<select class="form-control" name="' . $name . '">' . HTML::selectOptions($d['multi'], $value, false, 'value', 'name') . '</select>';
          if (!empty($d['description'])) {
            $html .= '<span class="help-block">' . $d['description'] . '</span>';
          }
        }
      }
        break;

      # Однострочное текстовое поле
      case Dynprops::typeInputText: {
        $value = (isset($d['value']) ? $d['value'] : $d['default_value']);
        $html = '<input type="text" class="form-control" name="' . $name . '" value="' . HTML::escape($value) . '" class="input-block-level" />';
        if (!empty($d['description'])) {
          $html .= '<span class="help-block">' . $d['description'] . '</span>';
        }

      }
        break;

      # Многострочное текстовое поле
      case Dynprops::typeTextarea: {
        $value = (isset($d['value']) ? $d['value'] : $d['default_value']);
        $html = '<textarea name="' . $name . '" rows="5" class="form-control" autocapitalize="off">' . HTML::escape($value) . '</textarea>';
        # уточнение к названию
        if (!empty($d['description'])) {
          $html .= '<span class="help-block">' . $d['description'] . '</span>';
        }
      }
        break;

      # Число
      case Dynprops::typeNumber: {
        $value = (isset($d['value']) ? $d['value'] : $d['default_value']);
        if (empty($d['description'])) {
          $html = '<input type="text" name="' . $name . '" value="' . $value . '" class="form-control" pattern="[0-9\.,]*" />';
        } else {
          if (mb_strlen(strip_tags($d['description'])) <= 5) {
            $html = '<div class="input-group">
                                    <input type="text" class="form-control" name="' . $name . '" value="' . $value . '" class="input-small" pattern="[0-9\.,]*" />
                                    <span class="input-group-addon">' . $d['description'] . '</span>
                                 </div>';
          } else {
            $html = '<input type="text" class="form-control" name="' . $name . '" value="' . $value . '" class="input-small" pattern="[0-9\.,]*" />';
            $html .= '<div class="help-block">' . $d['description'] . '</div>';

          }
        }
      }
        break;

      # Диапазон
      case Dynprops::typeRange: {
        $value = (isset($d['value']) && $d['value'] ? $d['value'] : $d['default_value']);

        $html = '<select class="form-control" name="' . $name . '" class="input-small">';
        if (!empty($value) && !intval($value)) {
          $html .= '<option value="0">' . $value . '</option>';
        }
        if ($d['start'] <= $d['end']) {
          for ($i = $d['start']; $i <= $d['end']; $i += $d['step']) {
            $html .= '<option value="' . $i . '"' . ($value == $i ? ' selected="selected"' : '') . '>' . $i . '</option>';
          }
        } else {
          for ($i = $d['start']; $i >= $d['end']; $i -= $d['step']) {
            $html .= '<option value="' . $i . '"' . ($value == $i ? ' selected="selected"' : '') . '>' . $i . '</option>';
          }
        }
        $html .= '</select>';

        # уточнение к названию
        if (!empty($d['description'])) {
          $html .= '<div class="help-block">' . $d['description'] . '</div>';
        }
      }
        break;
    }

    $drawControl($d['title'], $html, $d['req'], $class, $extra);
  }
};
# ---------------------------------------------------------------------------------------
# Настройки дин. св-в категории
$drawDynprops($this, false, false, true);

# ---------------------------------------------------------------------------------------
# Вывод и настройки дин. св-в категории
$hide_publisher_dp = ' class="hide" ';
# Отображаем collapse при наличии дин. св-в
if ($extra['existsPublisherGroupDP']){
    $hide_publisher_dp = '';
}
# Скрываем при выключенной настройке отображения в collapse
if (isset($extra['publisher_type_from_shop'])
    && !empty($extra['publisher_type_from_shop'])
    && !Vinylhandler::isCollapsePublisherDynPropInItemType($extra['publisher_type_from_shop'])){
    $hide_publisher_dp = ' class="hide" ';
}
echo('<div'.$hide_publisher_dp.' data-dp-type="'.Vinylhandler::DYN_PROP_TYPE_PUBLISHER.'">
        <div class="row">
            <div class="col-sm-3 control-label"></div>
            <p  class="form-btn-collapse pdl15"
                data-toggle="collapse"
                data-target="#'.Vinylhandler::DYN_PROP_TYPE_PUBLISHER.'"
              >
                '._t('bbs-item-dp-base', 'Указать дополнительные характеристики').'
                <b class="l-header-btn-text caret"></b>
            </p>
        </div>
      </div>');
# Дин. свойства (вне очереди):
echo('<div id="'.Vinylhandler::DYN_PROP_TYPE_PUBLISHER.'" class="collapse ' . ($hide_publisher_dp ? 'in' : '') . '">');
$drawDynprops($this, true);
# Дин. свойства (по порядку):
$drawDynprops($this, false);
echo('</div>');

# ---------------------------------------------------------------------------------------
# Вывод и настройки дин. св-в базовой категории группы "0"
$hide_base_dp = ' class="hide" ';
# Отображаем collapse при наличии дин. св-в
if ($extra['existsBaseGroupDP']){
    $hide_base_dp = '';
}
# Скрываем при выключенной настройке отображения в collapse
if (isset($extra['publisher_type_from_shop'])
    && !empty($extra['publisher_type_from_shop'])
    && !Vinylhandler::isCollapseBaseDynPropInItemType($extra['publisher_type_from_shop'])){
    $hide_base_dp = ' class="hide" ';
}
echo('<div'.$hide_base_dp.' data-dp-type="'.Vinylhandler::DYN_PROP_TYPE_BASE.'">
        <div class="row">
            <div class="col-sm-3 control-label"></div>
            <p  class="form-btn-collapse pdl15"
                data-toggle="collapse"
                data-target="#'.Vinylhandler::DYN_PROP_TYPE_BASE.'"
              >
                '._t('bbs-item-dp-base', 'Указать способ доставки и оплаты').'
                <b class="l-header-btn-text caret"></b>
            </p>
        </div>
      </div>');
echo('<div id="'.Vinylhandler::DYN_PROP_TYPE_BASE.'" class="collapse ' . ($hide_base_dp ? 'in' : '') . '">');
$drawDynprops($this, false, true);
echo('</div>');
