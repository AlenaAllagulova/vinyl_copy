<?php use bff\db\Dynprops;

/**
 * Просмотр объявления: блок дин. свойств
 * @var $this bff\db\Dynprops
 * @var $dynprops array основные дин. свойства
 * @var $children array вложенные дин. свойства
 */

# Добавили настройки/содержимое категории для свойства "год выпуска"
if (!empty($extra['current_category_data']) && $extra['current_category_data']['year_enabled']) {
    if (!empty($extra['current_item_data']) && !empty($extra['current_item_data']['year'])) {
        $dynprops = array_merge(
            [[
                'title' => $extra['current_category_data']['year_title'],
                'value' => $extra['current_item_data']['year'],
                'num_first' => false,
                'group_id'  => Vinylhandler::PUBLISHER_TYPES[$extra['current_category_data']['year_show_in']],
                'type' => Dynprops::typeNumber,
                'description' => '',
            ]],
            $dynprops
        );
    }
}
# Настройки доступности дин.св-в базовой группы для типов ОБ от магазина / частное(простое)
$enabledBaseDP = true;
if(isset($extra['current_item_data']['publisher_type_from_shop']) && !empty($extra['current_item_data']['publisher_type_from_shop'])){
    $enabledBaseDP = Vinylhandler::isUseBaseDynPropInItemType($extra['current_item_data']['publisher_type_from_shop']);
}

/**
 * Отрисовка свойства
 * @param string $title название свойства
 * @param mixed $value значение свойства
 * @param string $description доп. описание
 */
$drawProperty = function($title, $value, $description = '') {
  if( mb_strlen(strip_tags($description)) > 5 ) {
    $description = '';
  }
  ?>
  <div class="ad-dynprops-item">
    <div class="ad-dynprops-item-in">
      <span class="ad-dynprops-item-attr"><?= $title ?>:</span>
      <span class="ad-dynprops-item-val"><?= $value ?>&nbsp;<?= $description ?></span>
    </div>
  </div>
  <?
};

/**
 * Отрисовка дин. свойств
 * @param bff\db\Dynprops $self
 * @param boolean $numFirst дин. св-ва помеченные вне очереди (num_first = 1)
 * @param boolean $numBaseGroup дин. св-ва базавой группы (group_id = "0")
 */
$drawDynprops = function($self, $numFirst = false, $numBaseGroup = false) use (&$dynprops, $drawProperty, $children, $extra)
{
  foreach($dynprops as $d)
  {
      if (empty($d['value'])) continue;
      if (($numFirst && !$d['num_first']) || (!$numFirst && $d['num_first'])
      ) continue;
      if ($numBaseGroup && ($d['group_id'] !== Vinylhandler::DYN_PROP_BASE_GROUP) ||
          !$numBaseGroup && ($d['group_id'] === Vinylhandler::DYN_PROP_BASE_GROUP)
      ) continue;

      if(isset($extra['current_item_data']['publisher_type_from_shop']) && !empty($extra['current_item_data']['publisher_type_from_shop'])){
          if ($d['group_id'] !== Vinylhandler::DYN_PROP_BASE_GROUP
              && $d['group_id'] !== $extra['current_item_data']['publisher_type_from_shop']
              && $d['group_id'] !== Vinylhandler::DYN_PROP_GROUP_SIMPLE_SHOP
          ) {
              continue;
          }
      }

      $value = $d['value'];

    switch($d['type'])
    {
      case Dynprops::typeRadioGroup:
      {
        foreach($d['multi'] as $dm) {
         if($dm['value'] == $value) {
           $value = $dm['name'];
           break;
         }
       }
     }break;
     case Dynprops::typeRadioYesNo:
     {
      $value = ($value == 2 ? $self->langText['yes'] : ($value == 1 ? $self->langText['no'] : ''));
    }break;
    case Dynprops::typeCheckboxGroup:
    {
      $value = explode(';', $value);
      $res = array();
      foreach($d['multi'] as $dm) {
       if(in_array($dm['value'], $value))
         $res[] = $dm['name'];
     }
     $value = join(', ', $res);
   }break;
   case Dynprops::typeCheckbox:
   {
    $value = ($value ? $self->langText['yes'] : $self->langText['no']);
  }break;
  case Dynprops::typeSelect:
  {
    if($d['parent'])
    {
      foreach($d['multi'] as $dm) {
        if($value == $dm['value']) {
          $drawProperty($d['title'], $dm['name']);
          break;
        }
      }
      if( ! empty($value) && isset($children[$d['id']])) {
        $dmv = current( $children[$d['id']] );
        foreach($dmv['multi'] as $dm) {
          if($dm['value'] == $dmv['value']) {
            $drawProperty($d['child_title'], $dm['name']);
            break;
          }
        }
      }
      continue 2;
    } else {
      foreach($d['multi'] as $dm) {
        if($value == $dm['value']) {
          $value = $dm['name'];
          break;
        }
      }
    }
  }break;
  case Dynprops::typeInputText:
  case Dynprops::typeTextarea:
  case Dynprops::typeNumber:
  case Dynprops::typeRange:
  {
                # $value = $d['value'];
  } break;
}

if( ! empty($value) ) {
  $drawProperty($d['title'], $value, $d['description']);
}
}

};

# ---------------------------------------------------------------------------------------
# Дин. свойства (вне очереди):
$drawDynprops($this, true);
# ---------------------------------------------------------------------------------------
# Дин. свойства (по порядку):
$drawDynprops($this, false);

# ---------------------------------------------------------------------------------------
# Дин. свойства (базовой категории, базовой группы "0"):
if($enabledBaseDP){
/*echo('<div class="vinyl-dp">'); */
$drawDynprops($this, false, true);
/*echo('</div>');  */
}