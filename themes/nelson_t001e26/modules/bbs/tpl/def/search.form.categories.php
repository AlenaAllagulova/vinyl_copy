<div class="l-filter-form-item dropdown">
  <a class="l-filter-form-btn" href="#" id="j-f-cat-desktop-link">
    <?= ( $catACTIVE ? (DEVICE_DESKTOP_OR_TABLET ? $catData['dropdown']['title'] : $catData['title']) : _t('bbs','Все категории') ) ?>
    <b class="caret"></b>
  </a>
  <div id="j-f-cat-desktop-popup" class="l-categories-dropdown dropdown-menu">
    <div id="j-f-cat-desktop-step1"<? if($catACTIVE_STEP != 1) { ?> class="hide"<? } ?>>
      <?= BBS::i()->catsList('search', bff::DEVICE_DESKTOP, 0); ?>
    </div>
    <div id="j-f-cat-desktop-step2"<? if($catACTIVE_STEP != 2) { ?> class="hide"<? } ?>>
      <? if($catACTIVE_STEP == 2) echo BBS::i()->catsList('search', bff::DEVICE_DESKTOP, (isset($catData['dropdown']['id']) ? $catData['dropdown']['id'] : $catID)); ?>
    </div>
  </div>
</div>