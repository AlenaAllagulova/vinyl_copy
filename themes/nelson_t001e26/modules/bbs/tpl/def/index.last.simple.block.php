<?php
/**
 * Блок объявлений на главной
 * @var $this BBS
 * @var $title string заголовок блока
 * @var $items array данные об объявлениях
 * @var $type string тип объявлений: 'last', 'premium'
 * @var $cnt_total int кол-во объявлений при инициализации страницы
 * @var $cnt_limit_init int доступное кол-во объявлений
 */
$lng_fav_in = _te('bbs', 'Добавить в избранное');
$lng_fav_out = _te('bbs', 'Удалить из избранного');
$lng_quick = _t('bbs', 'срочно');
?>
<div id="j-bbs-index-<?= $type ?>-block">
    <div class="index__heading__title mrgt30 mrgb20 text-center index-article-<?= $type ?>">
        <?= $title ?>
    </div>

  <div class="gallery-box <?= $type ?> j-box-<?= $type ?>">
    <?php foreach($items as &$v) {
      // Gallery Item Template
      echo View::template('search.item.gallery', array('item'=>&$v), 'bbs');
    } unset ($v); ?>
  </div>
</div>
<? if (($cnt_total > $cnt_limit_init) && in_array($type, ['premium', 'last'])): ?>
  <div class="text-center mrgt20">
      <span
        style="display: inline-block;cursor: pointer;"
        type="button"
        class="l-filter-form-submit l-filter-form-submit_mobile  j-last_items-load"
        data-ev="ajax"
        data-offset="<?= Vinylhandler::getBbsItemsLimitInit($type); ?>"
        data-target=".j-box-<?= $type ?>"
        data-type="<?= $type ?>"
        data-page="index"
      >
        <?= _t('','Еще объявления') ?>
      </span>
  </div>
<? endif; ?>
