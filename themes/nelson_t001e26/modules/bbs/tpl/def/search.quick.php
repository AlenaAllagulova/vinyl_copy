<?php

/**
 * Быстрый поиск объявлений: список (desktop, tablet)
 * @var $this BBS
 * @var $items array объявления
 */

$lang_photo = _te('bbs', 'фото');
$aGetParams = ['q' => $q, 'mq'=> $q];
$aSearchParam = [];
if ($f['c'] > 0 && config::sysAdmin('bbs.search.quick.category', false, TYPE_BOOL)) {
    $aGetParams['c'] = $f['c'];
    $catFields = array(
        'id',
        'numlevel',
        'keyword',
        'landing_url',
        'enabled',
    );
    $aCatData= $this->model->catData($f['c'],$catFields);
    $aSearchParam = ['keyword' => $aCatData['keyword']];
}
$sGetParams = http_build_query($aGetParams);
$link_all_items = BBS::url('items.search', $aSearchParam).'?'.$sGetParams;
?>
<? foreach($items as $v){ ?>
<a href="<?= $v['link'] ?>" class="l-filter-qsearch-item">
  <span class="l-filter-qsearch-item-title">
    <span class="l-filter-qsearch-item-title-name"><?= $v['title'] ?></span>
    <? if($v['price_on']) { ?><span class="l-filter-qsearch-item-title-price"><?= $v['price'] ?></span><? } ?>
  </span>
  <span class="l-filter-qsearch-item-img">
    <?
    foreach ($v['img'] as $i):
      echo '<img src="'.$i.'" title="'.$v['title'].' '.$lang_photo.'" />';
    endforeach;
    ?>
  </span>
</a>
<? } ?>
<?if($cnt):?>
    <a href="<?= $link_all_items; ?>" class="l-filter-qsearch-item">
      <span class="l-filter-qsearch-item-title">
        <span class="l-filter-qsearch-item-title-name"><?= _t('bbs_quick_search', 'Все объявления') ?></span>
      </span>
    </a>
<?endif;?>
