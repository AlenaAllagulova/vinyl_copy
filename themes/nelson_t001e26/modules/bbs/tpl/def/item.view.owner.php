<?php

/**
 * Просмотр объявления: блок автора объявления
 * @var $this BBS
 * @var $user array данные пользователя (автора)
 * @var $owner boolean авторизованный пользователь = автор просматриваемого объявления
 * @var $contacts array контактные данные
 * @var $shop_id integer ID магазина
 * @var $shop array данные о магазине
 */

?>

<div class="ad-author">

  <? // Shop
  if($shop_id && $shop) { ?>
    <div class="ad-author-in ad-author-shop">
      <? if($shop['logo']) { ?>
      <div class="ad-author-shop-logo">
        <a href="<?= $shop['link'] ?>">
          <img src="<?= $shop['logo'] ?>" alt="" />
        </a>
      </div>
      <? } ?>
      <div class="ad-author-shop-name">
        <a href="<?= $shop['link'] ?>" title="<?= $shop['title'] ?>"><?= $shop['title'] ?></a>
<?php bff::hook('plugin.user_online_do.block', $user['id'], 'page_itemShopView') ?>
      </div>
      <div class="ad-author-shop-descr">
        <? if(($descr_limit = 100) && mb_strlen($shop['descr']) > $descr_limit) { ?>
          <div><?= tpl::truncate($shop['descr'], $descr_limit ,'', true) ?><a href="#" class="link-ajax" id="j-view-owner-shop-descr-ex"><span>...</span></a></div>
          <div class="hide"><?= mb_substr($shop['descr'], $descr_limit); ?></div>
          <script type="text/javascript">
            <? js::start() ?>
            $(function(){
              $('#j-view-owner-shop-descr-ex').on('click', function(e){ nothing(e);
                var $content = $(this).parent(); $(this).remove();
                $content.html($content.text() + $content.next().text());
              });
            });
            <? js::stop() ?>
          </script>
        <? } else { ?>
          <?= $shop['descr']; ?>
        <? } ?>
      </div>
      <? if( ! empty($shop['site'])) { ?>
      <div class="ad-author-shop-website">
        <a href="<?= bff::urlAway($shop['site']) ?>" target="_blank" rel="nofollow" class="link-ico j-away"><i class="fa fa-globe"></i> <span><?= $shop['site'] ?></span></a>
      </div>
      <? } ?>
    <? if($shop_id && $shop && ! empty($shop['social'])) { ?>
    <? $social = Shops::socialLinksTypes(); ?>
    <div class="ad-author-contact-social">
      <? foreach($shop['social'] as $v):
        if ($v && isset($social[$v['t']])) {
      ?>
        <a class="sh-social sh-social_<?= $social[$v['t']]['icon'] ?>" href="<?= bff::urlAway($v['v']) ?>" rel="nofollow noreferrer noopener" target="_blank"></a><?
      } endforeach; ?>
    </div>
    <? } ?> <!--
      <?php if( ! empty($shop['addr_addr'])) { ?>
      <div class="ad-author-shop-location">
        <?php $addr_map = ( floatval($shop['addr_lat']) && floatval($shop['addr_lon']) );
           if ($addr_map) {
                Geo::mapsAPI(false);
           }
        ?>
        <div class="ad-author-shop-location-info"><?= $shop['region_title'].', '.$shop['addr_addr'] ?></div>
        <?php if($addr_map) { ?>
        <a href="#" class="link-ajax" id="j-view-owner-shop-map-toggler"><i class="fa fa-map-marker"></i> <span><?= _t('view', 'Показать на карте') ?></span></a>
        <div id="j-view-owner-shop-map-popup" class="ad-author-map" style="display: none;">
          <div id="j-view-owner-shop-map-container" class="ad-author-map-container"></div>
          <script type="text/javascript">
            <?php js::start() ?>
            $(function(){
              var jViewShopMap = (function(){
                var map = false;
                app.popup('view-shop-map', '#j-view-owner-shop-map-popup', '#j-view-owner-shop-map-toggler', {
                  onShow: function($p){
                    $p.fadeIn(100, function(){
                      if (map) {
                        map.panTo([<?= HTML::escape($shop['addr_lat'].','.$shop['addr_lon'], 'js') ?>], {delay: 10, duration: 200});
                      } else {
                        map = app.map('j-view-owner-shop-map-container', '<?= HTML::escape($shop['addr_lat'].','.$shop['addr_lon'], 'js') ?>', false, {
                          marker: true,
                          zoom: 12,
                          controls: 'view'
                        });
                      }
                    });
                  }
                });
              }());
            });
            <?php js::stop() ?>
          </script>
        </div>
        <?php } ?>
      </div>
      <?php } ?>-->
    </div>
  <? } else { ?>
    <div class="ad-author-in ad-author-user">
      <a href="<?= $user['link'] ?>" class="ad-author-user-avatar">
        <img src="<?= $user['avatar'] ?>" alt="" />
      </a>
      <div class="ad-author-user-info">
        <div class="ad-author-user-name"><?= $name ?></div>
<?php bff::hook('plugin.user_online_do.block', $user['id'], 'page_itemView') ?>
        <? if($owner_type == BBS::OWNER_PRIVATE) { ?>
          <div class="ad-author-user-type"><?= _t('view', 'частное лицо') ?></div>
        <? } ?>
        <? if($user['created']!=='0000-00-00 00:00:00') { ?>
          <div class="ad-author-user-created"><?= _t('view', 'на сайте с [date]', array('date'=>tpl::date_format2($user['created']))) ?></div>
        <? } ?>
        <div class="ad-author-user-all">
          <a href="<?= $user['link'] ?>"><?= _t('view', 'Все объявления автора') ?></a>
        </div>
      </div>
    </div>
  <? } ?>

  <? if($contacts['has'] || ! empty($shop['social'])): ?>
  <div class="ad-author-in ad-author-contact">
    <div class="ad-author-contact-row">
      <div class="ad-author-contact-row-label">
        <?= _t('view', 'Контакты:') ?>
      </div>
      <div class="ad-author-contact-row-content">
        <a href="#" class="link-ajax j-v-contacts-expand-link"><span><?= _t('view', 'показать контакты') ?></span></a>
      </div>
    </div>
    <div class="j-v-contacts-expand-block">
      <? if( ! empty($contacts['phones']) ) { ?>
      <div class="ad-author-contact-row">
        <div class="ad-author-contact-row-label">
          <?= _t('view', 'Тел.') ?>:
        </div>
        <div class="ad-author-contact-row-content j-c-phones">
          <? foreach($contacts['phones'] as $v) { ?><div><?= $v ?></div><? } ?>
        </div>
      </div>
          <?php if (!$owner): ?>
          <div class="control-user">
              <div class="collapse list-contact" id="list-contact">
                  <button class="close j-toggle-body" type="button" data-toggle="collapse" data-target="#list-contact" aria-expanded="false" aria-controls="list-contact">
                      <i class="fa fa-times-circle" aria-hidden="true"></i>
                  </button>
                  <div class="text-center pdb10 ">
                      <h4 class="">
                          <? if($shop_id && !empty($shop)): ?>
                                <?= $shop['title'] ?>
                          <? else: ?>
                              <?= $user['name']?>
                          <? endif; ?>
                      </h4>
                      <div class="list-contact__article">
                          <?= _t('','Выберите номер телефона')?>
                      </div>
                      <div class="j-c-phones ">
                      <? foreach($contacts['phones'] as $v) { ?>
                          <span class="list-contact__item "><?= $v ?></span>
                      <? } ?>
                      </div>
                  </div>
              </div>
              <a href="<?= (User::id()? '#contact-form' : Users::url('login'))?>" class="btn btn-message j-save-for-message">
                  <i class="fa fa-envelope" aria-hidden="true"></i>
                  <?= _t('','написать ')?>
              </a>
              <button class="btn btn-call j-show-conatct j-toggle-body" type="button" data-toggle="collapse" data-target="#list-contact" aria-expanded="false" aria-controls="list-contact">
                  <i class="fa fa-phone" aria-hidden="true"></i>
                  <?= _t('','позвонить')?>
              </button>
          </div>
          <? endif; ?>
      <? } # phones ?>
      <? if(!empty($contacts['contacts'])): ?>
        <?php foreach (Users::contactsFields($contacts['contacts']) as $contact): ?>
          <div class="ad-author-contact-row">
            <div class="ad-author-contact-row-label"><?= $contact['title'] ?>:</div>
            <div class="ad-author-contact-row-content j-c-<?= $contact['key'] ?>">
              <?= tpl::contactMask($contact['value']) ?>
            </div>
          </div>
        <?php endforeach; ?>
      <? endif; ?>
    </div>
  </div><!-- /.ad-author-contact -->
  <? else: # $contacts['has'] ?>
    <?php if (!$owner): ?>
        <div class="control-user">
            <a href="<?= (User::id()? '#contact-form' : Users::url('login'))?>" class="btn btn-message j-save-for-message">
                <i class="fa fa-envelope" aria-hidden="true"></i>
                <?= _t('','написать ')?>
            </a>
        </div>
    <? endif; ?>
  <? endif; ?>
  <?php if ( ! $owner && DEVICE_DESKTOP) { ?>
  <div class="ad-author-in">

    <a class="btn btn-block btn-info <?= (User::id()? '' : 'j-save-for-message')?>" href="<?= (User::id()? '#contact-form' : Users::url('login'))?>">
        <i class="fa fa-envelope"></i>
        <?= _t('view', 'Написать автору') ?>
    </a>
  </div>
  <? } ?>
  <?php if ( ! empty($share_code)) { ?>
  <div class="ad-author-in">
    <div class="text-center">
      <?= $share_code ?>
    </div>
  </div>
  <?php } ?>
  <?php if (DEVICE_DESKTOP) { ?>
  <div class="ad-author-in ad-author-stats">
    <div class="ad-author-stats-item">
      <?= _t('view', '[views_total] объявления', array('views_total'=>tpl::declension($views_total, _t('view', 'просмотр;просмотра;просмотров')))) ?>
    </div>
    <div class="ad-author-stats-item">
      <?= _t('view', '[views_today] из них сегодня', array('views_today'=>$views_today)) ?>
    </div>
    <div class="ad-author-stats-item">
      <?php if($views_total) { ?><a href="#" class="link-ajax" id="j-v-viewstat-desktop-link-2"><span><?= _t('view', 'Посмотреть статистику') ?></span></a><?php } ?>
    </div>
    <div id="j-v-viewstat-desktop-popup-container"></div>
  </div>
  <? } ?>
</div><!-- /.ad-author -->

<script type="text/javascript">
    <? js::start(); ?>
    $(function () {
        $('.j-save-for-message').on('click', function (e) {
            e.preventDefault();
            localStorage.setItem('scrollMessage', 'true');
            location.href = $(this).attr('href');
        });

        if(localStorage.getItem('scrollMessage')) {
            localStorage.removeItem('scrollMessage');
            setTimeout(function () {
                $([document.documentElement, document.body]).animate({
                    scrollTop: $(".l-content-box-in.contact").offset().top - 50
                }, 1000);
                focusInput();
            }, 500);
        }
        function focusInput () {
            document.getElementById('j-textarea-message').focus();
        }

        $('.j-toggle-body').on('click', function () {
            $('body').toggleClass('bg-hide');
        })
    });
    <? js::stop(); ?>
</script>

