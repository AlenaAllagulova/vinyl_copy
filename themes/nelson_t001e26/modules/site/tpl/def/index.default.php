<div class="index-categories">

  <?php $i = 1; foreach($cats as $k=>$v) { if( $i == 1 ) { ?>
    <div class="row">
    <?php } ?>
    <div class="col-sm-4">
      <div class="index-categories-item <?= $i%2 ?>">
        <div class="index-categories-item-img">
          <?php if ($v['items'] > 0) { ?>
          <a href="<?= $v['l'] ?>" class="img"><img src="<?= $v['i'] ?>" alt="<?= $v['t'] ?>" /></a>
          <?php } else { ?>
          <span data-link="<?= $v['l'] ?>" class="img hidden-link"><img src="<?= $v['i'] ?>" alt="<?= $v['t'] ?>" /></span>
          <?php } ?>
        </div>
        <div class="index-categories-item-content">
          <div class="index-categories-item-title">
            <?php if ($v['items'] > 0) { ?>
            <a href="<?= $v['l'] ?>"><?= $v['t'] ?></a>
            <?php } else { ?>
            <span class="hidden-link" data-link="<?= $v['l'] ?>"><?= $v['t'] ?></span>
            <?php } ?>
            <span class="index-categories-item-title-count"><?= $v['items'] ?></span>
          </div>
          <?php if($v['subn']): ?>
            <ul class="index-categories-item-links">
              <?php $j = 0; foreach($v['sub'] as $vv) { ?>
                <li>
                    <?php if ($vv['items'] > 0) { ?>
                    <a href="<?= $vv['l'] ?>"><?= $vv['t'] ?></a>
                    <?php } else { ?>
                    <span class="hidden-link" data-link="<?= $vv['l'] ?>"><?= $vv['t'] ?></span>
                    <?php } ?>
                </li>
              <?php } ?>
            </ul>
          <?php endif; ?>
        </div>
      </div>
    </div>
      <?php if( $i++ == 3 ) { ?></div><?php $i = 1; } };
      if( $i!=1 ) { ?>
    </div>
  <?php } ?>

</div><!-- /.index-categories -->