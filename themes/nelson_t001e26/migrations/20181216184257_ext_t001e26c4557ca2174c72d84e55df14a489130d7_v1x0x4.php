<?php

use bff\db\migrations\Migration as Migration;

class ExtT001e26c4557ca2174c72d84e55df14a489130d7V1x0x4 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $this->table(TABLE_BBS_CATEGORIES)
            ->addColumn('photos_shop', 'integer', [
                'signed' => false,
                'limit' => 2,
                'null'  => false,
                'default' => BBS::itemsImagesLimit(false)
                ])
            ->update();
    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        $this->table(TABLE_BBS_CATEGORIES)
            ->removeColumn('photos_shop')
            ->update();
    }
}