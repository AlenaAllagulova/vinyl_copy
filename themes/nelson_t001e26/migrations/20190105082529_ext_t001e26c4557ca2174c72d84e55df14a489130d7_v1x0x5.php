<?php

use bff\db\migrations\Migration as Migration;

class ExtT001e26c4557ca2174c72d84e55df14a489130d7V1x0x5 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $this->table(TABLE_BBS_CATEGORIES)
            ->addColumn('year_show_in', 'integer', [
                'signed' => false,
                'limit' => 1,
                'null'  => false,
                'default' => Vinylhandler::DYN_PROP_SHOW_IN_SIMPLE_SHOP,
            ])
            ->update();
    }
    
    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        $this->table(TABLE_BBS_CATEGORIES)
            ->removeColumn('year_show_in')
            ->update();
    }
}