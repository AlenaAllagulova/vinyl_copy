<?php

use bff\db\migrations\Migration as Migration;

class ExtT001e26c4557ca2174c72d84e55df14a489130d7V1x0x1 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $this->table('bff_bbs_items')
            ->addColumn('year', 'integer', ['signed' => false, 'null' => true, 'default' => null])
            ->update();
    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        $table = $this->table('bff_bbs_items');
        $table->removeColumn('year')
            ->update();
    }
}
