<?php

use bff\db\migrations\Migration as Migration;

class ExtT001e26c4557ca2174c72d84e55df14a489130d7V1x0x3 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        $this->table(TABLE_BBS_CATEGORIES)
            ->addColumn('year_enabled', 'boolean', ['null'  => false, 'default' => false])
            ->update();
    
        $this->table(TABLE_BBS_CATEGORIES_LANG)
            ->addColumn('year_title', 'string', ['null'  => true])
            ->update();
    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Table::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        $this->table(TABLE_BBS_CATEGORIES)
            ->removeColumn('year_enabled')
            ->update();
    
        $this->table(TABLE_BBS_CATEGORIES_LANG)
            ->removeColumn('year_title')
            ->update();
    }
}
