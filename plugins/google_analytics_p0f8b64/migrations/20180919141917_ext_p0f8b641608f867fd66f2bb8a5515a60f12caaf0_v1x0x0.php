<?php

use bff\db\migrations\Migration as Migration;

class ExtP0f8b641608f867fd66f2bb8a5515a60f12caaf0V1x0x0 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        if (!$this->hasTable($this->extension->table())) {
            $this->table($this->extension->table(), ['engine' => 'InnoDB', 'id' => false, 'primary_key' => ['id']])
                  ->addColumn('id', 'integer', ['signed' => false, 'identity' => true])
                  ->addColumn('name', 'string', ['null' => false, 'default' => '', 'limit' => 255])
                  ->addColumn('value', 'string', ['null' => false, 'default' => '', 'limit' => 255])
                  ->create();
        }
    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Migration::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        Migration::dropIfExists($this->extension->table());
    }
}