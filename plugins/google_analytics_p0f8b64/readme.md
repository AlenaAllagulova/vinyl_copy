Вы можете посмотреть [видеоинструкцию](https://www.youtube.com/watch?v=rnUGssC3800) или воспользоваться инструкцией ниже:

- Перейдите на [страницу плагина](?s={plugin:name}&ev=listing).
- В окне настроек нажмите кнопку **Получить код доступа**:
![alt step1]({plugin:url}/img/step1.jpg)

- В открывшемся окне выберите нужный аккаунт:
![alt step2]({plugin:url}/img/step2.jpg)

- Нажмите **Разрешить** в окне с запросом просматривать данные Google Analytics плагину **GA Plugin**:
![alt step3]({plugin:url}/img/step3.jpg)

- Скопируйте сформированный код:
![alt step4]({plugin:url}/img/step4.jpg)

- Вставьте скопированный код в поле **Код доступа**:
![alt step5]({plugin:url}/img/step5.jpg)

- Установите нужные параметры и сохраните настройки:
![alt step6]({plugin:url}/img/step6.jpg)
