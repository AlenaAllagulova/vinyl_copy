<?php
/**
 * @var $without_region array
 * @var $list array
 */
?>
<tr>
    <td width="15"></td>
    <td class="left">
        <small><?= $this->langAdmin('Без региона'); ?></small>
    </td>
    <td width="165">
        <small><?= $without_region[0]['counter']; ?></small>
    </td>
    <td width="35">
        <small><?= $without_region[0]['counter'] - $without_region[0]['hits']; ?></small>
    </td>
    <td width="215">
        <small><?= tpl::date_format3($without_region[0]['last_time'], 'd.m.Y H:i'); ?></small>
    </td>
</tr>
<?php foreach($list as $k=>$v) { ?>
    <tr>
        <td ><small><?= $k+1 ?></small></td>
        <td class="left">
            <small><?= Geo::regionData($v['region_id'])['title_ru']; ?></small>
        </td>
        <td>
            <small><?= $v['counter']; ?></small>
        </td>
        <td>
            <small><?= $v['counter'] - $v['hits']; ?></small>
        </td>
        <td>
            <small><?= tpl::date_format3($v['last_time'], 'd.m.Y H:i'); ?></small>
        </td>
    </tr>
<?php } ?>