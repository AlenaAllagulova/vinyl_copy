<?php
$aData['submitButtons'] = '';
tpl::includeJS(array('datepicker'), true);
tplAdmin::adminPageSettings(array(
    'fordev'=>array(
        'links-rebuild' => array('title'=>$this->langAdmin('очистить таблицу'), 'class'=>'j-remove-all', 'icon'=>'icon-check'),
    ),
));
?>
<style type="text/css">
    .tooltip-inner { width: auto !important; max-width: 100% !important; }
    .tabsBar .tab a { text-decoration: none; }
    .j-plugin-fquerys-tooltip {cursor: pointer;}
    .progress { position: absolute; right: 10px; top: 20px;  }

    /* раскрываемый список регионов */
    #items-list .view { cursor: pointer; }
	#items-list .fold { display: none; }
	#items-list .fold > td { border-top: none; padding: 0 0 0 8px; }
	#items-list .fold.open { display: table-row; border-top: 1px dotted #ddd; background: #f5f5f5; }
	
	/* регионы */
    .r-sc { max-height: 200px; overflow-y: scroll; overflow-x: hidden; position: relative; }
	.j-sub-list > tr:before { content: ""; display: block; position: absolute; width: 9px; left: 18px;
	    border-top: 1px dotted #ccc; margin-top: 10px; }
	.j-sub-list > tr:after { content: ""; display: block; position: absolute; left: 16px;
		border-left: 1px dotted #ccc; height: 19px; }
	.j-sub-list tr:hover:before { border-top: 1px dotted #666; }
	.j-sub-list:hover tr:after { border-left: 1px dotted #666; }
</style>

<div class="actionBar">
    <form action="" method="get" name="filters" id="items-filters" class="form-inline">
        <input type="hidden" name="s" value="<?= $this->getName(); ?>" />
        <input type="hidden" name="ev" value="listing" />
        <input type="hidden" name="page" value="<?= $f['page'] ?>" />
        <input id="order" type="hidden" name="order" value="desc" />
        <input id="col" type="hidden" name="col" value="last_time" />
        <div class="controls controls-row">
            <div class="left">
            	<label><input type="text" maxlength="150" name="q" value="<?= HTML::escape($f['q']) ?>" placeholder="<?= $this->langAdmin('Поисковый запрос'); ?>" style="width: 125px;" /></label>
            	<label style="margin-left: 10px;"><?= $this->langAdmin('Дата:'); ?> <input type="text" name="tfrom" value="<?= $f['tfrom'] ?>" placeholder="<?= $this->langAdmin('от'); ?>" style="width: 65px;" class="bff-datepicker" /></label><label>&nbsp;<input type="text" name="tto" value="<?= $f['tto'] ?>" placeholder="<?= $this->langAdmin('до'); ?>" style="width: 65px;" class="bff-datepicker" />&nbsp;</label>
            </div>
            <div class="left" style="margin-left: 4px;">
                <div class="btn-group">
	                <input type="submit" class="btn btn-small" onclick="jItems.submit(false);" value="<?= $this->langAdmin('найти'); ?>" />
	                <a class="btn btn-small" onclick="jItems.submit(true); return false;" title="<?= $this->langAdmin('сбросить'); ?>"><i class="disabled icon icon-refresh"></i></a>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
    <div id="j-mass" class="well well-small hide">
        <span class="j-info"></span>&nbsp;
        <input type="submit" class="btn btn-mini btn-danger button j-remove-checked" value="<?= $this->langAdmin('удалить выбранные'); ?>" />
    </div>
</div>

<table class="table table-hover admtbl">
	<thead>
	    <tr>
	        <th width="15"><label class="checkbox inline"><input type="checkbox" id="j-check-all" onclick="jItems.massModerate('check-all', this);" /></label></th>
	        <th class="left" style="padding-left: 10px;"><?= $this->langAdmin('Поисковые запросы'); ?></th>
	        <th width="100"><a onclick="jItems.order('counter', this); return false;"><?= $this->langAdmin('Всего'); ?> </a></th>
        	<th width="100"><?= $this->langAdmin('Неуспешных'); ?></th>
        	<th width="160"><a onclick="jItems.order('last_time', this); return false;"><?= $this->langAdmin('Последний поиск'); ?> <div id="order-ico" class="order-desc"></div></a></th>
        	<th width="20"></th>
	    </tr>
	</thead>
	<tbody id="items-list">
		<?= $list ?>
	</tbody>
</table>

<div id="items-pgn">
	<?= $pgn ?>
</div>

<script type="text/javascript">
var jItems = (function()
{
    var $progress, $listPgn, filters, $moderateList;
    var url = '<?= $this->adminLink('listing'); ?>';
    var $checkAll, $moderate, $moderateInfo;
    var _processing = false;
    var $list = $('#items-list');
    var order = 'desc', col = 'time';
    
    $(function(){
        $progress = $('#progress-items');
        $checkAll = $('#j-check-all');
        $moderate = $('#j-mass');
        $moderateInfo = $moderate.find('.j-info');
        $content  = $('#content-side');
        $moderateList = $('#items-moderate-list');
        $listPgn  = $('#items-pgn');
        filters   = $('#items-filters').get(0);

        $list.on('click', 'a.item-del', function(){
            var id = intval($(this).attr('rel'));
            if(id>0) del( id, this );
            return false;
        });

        $moderate.find('.j-remove-checked').click(function(){
	        var $ch = $list.find('.j-item-check:checked');
	        if(confirm('<?= $this->langAdmin('Удалить'); ?> ' + $ch.length + ' ' + bff.declension($ch.length,['<?= $this->langAdmin('запись'); ?>','<?= $this->langAdmin('записи'); ?>','<?= $this->langAdmin('записей'); ?>'], false) + '?')){
	            packDel('checked');
	        }
	    });

	    $content.find('.j-remove-all').click(function(){
	        if(confirm('<?= $this->langAdmin('Удалить все записи?'); ?>')){
	            packDel('all');
	        }
	    });

	    $('.j-plugin-fquerys-tooltip').tooltip();

        $list.on('click', 'tr.view', function(event){
		    if ($(event.target).is(':checkbox') || $(event.target).is('a')) {
                return;
            }
            if ($(this).hasClass('open')) {
                $(this).removeClass('open').next('.fold').removeClass('open');
            } else {
		    	var pid = intval($(this).data('id'));
                afterloadRegions(pid, 20);
                $(this).addClass('open').next('.fold').addClass('open');
            }
		});

        bff.datepicker('.bff-datepicker', {yearRange: '-3:+3'});

        setInterval(function(){
	        if(!$('input.j-item-check:visible:checked').length && !$('tr.open:visible').length) {
	            updateList();
	        }
	    }, <?= $this->config('update_time')*1000; ?>);
    });
    
    function isProcessing()
    {
        return _processing;
    }
    
    function del(id, link)
    {
        if(_processing) return false;
        _processing = true;

        bff.ajaxDelete('<?= $this->langAdmin('Удалить запись?'); ?>', id, url+'&act=delete&id='+id,
            link, {progress: $progress, repaint: false, onComplete: function(resp){
                if(resp && resp.success) {
	                bff.success('<?= $this->langAdmin('Запись удалена'); ?>');
	            }
                _processing = false;
                updateList();
            }});
        return false;
    }

    function packDel(mode)
    {
        if(_processing) return false;
        _processing = true;

        var $c = $('input.j-item-check:visible:checked:not(:disabled)');
        if(mode === 'checked')
        	var mess = '<?= _t('system', 'Операция выполнена успешно'); ?>';
        else if(mode === 'all')
        	var mess = '<?= _t('system', 'Операция выполнена успешно'); ?>';
        
        bff.ajax('<?= $this->adminLink('listing'); ?>'+'&act=delete-pack', $c.serialize()+'&mode='+mode+'&id=1' , function(resp){
            if(resp && resp.success) {
                bff.success(mess);
                jItems.massModerate('uncheck-all');
            }
            _processing = false;
            updateList();
        }, $progress);
    }

    function updateList()
    {
        if(isProcessing()) return;
        _processing = true;
        $list.addClass('disabled');
        var f = $(filters).serialize();
        bff.ajax(url, f, function(data){
            if(data) {
                $list.html( data.list );
                $listPgn.html( data.pgn );
                if(bff.h) {
                    window.history.pushState({}, document.title, url + '&' + f);
                }
            }
            $list.removeClass('disabled');
            _processing = false;
            
            $('.j-plugin-fquerys-tooltip').tooltip();
        }, $progress);
    }
    
    function setPage(id)
    {
        filters.page.value = intval(id);
    }

    return {
        submit: function(resetForm)
        {
            setPage(1);
            if(resetForm) {
                filters.q.value = '';
                filters.tfrom.value = '';
                filters.tto.value = '';
            }
            jItems.massModerate('uncheck-all');
            updateList();
        },
        order: function(col, url)
        {
            $('.order-'+order).remove();
            var div = document.createElement('div');

            if(order === 'desc') order = 'asc';
            else order = 'desc';

            div.className = 'order-'+order;
            url.appendChild(div);
            
            $('#order').val(order);
            $('#col').val(col);

            jItems.submit(false);
        },
        page: function (id)
        {
            if(isProcessing()) return false;
            setPage(id);
            updateList();
            return true;
        },
        massModerate: function(act, extra)
        {
            var $c = $('input.j-item-check:visible:checked:not(:disabled)');
            var $all = $('input.j-item-check:visible:not(:disabled)');
            var $allCheck = $('#j-check-all');

            switch(act) {
                case 'check-all': {
                    if(!$all.length) return false;
                    if(!$c.length || $c.length < $all.length) {
                        $all.prop('checked', true); // доотмечаем неотмеченные
                        $(extra).prop('checked', true);
                        $moderateInfo.html(bff.declension($all.length,['<?= $this->langAdmin('Отмечена'); ?>','<?= $this->langAdmin('Отмечено'); ?>','<?= $this->langAdmin('Отмечено'); ?>'], false)+' <strong>' + $all.length + '</strong> ' + bff.declension($all.length,['<?= $this->langAdmin('запись'); ?>','<?= $this->langAdmin('записи'); ?>','<?= $this->langAdmin('записей'); ?>'], false));
                        $moderate.show();
                    } else {
                        $all.prop('checked',false);
                        $(extra).prop('checked',false);
                        $moderate.hide();
                    }
                    return false;
                } break;
                case 'check': {
                    if(!$c.length || $c.length <= 0) {
                        $moderate.hide();
                    } else {
                        $moderateInfo.html(bff.declension($c.length,['<?= $this->langAdmin('Отмечена'); ?>','<?= $this->langAdmin('Отмечено'); ?>','<?= $this->langAdmin('Отмечено'); ?>'], false)+' <strong>' + $c.length + '</strong> ' + bff.declension($c.length,['<?= $this->langAdmin('запись'); ?>','<?= $this->langAdmin('записи'); ?>','<?= $this->langAdmin('записей'); ?>'], false));
                        $moderate.show();
                    }
                    
                    if(!$c.length || $c.length < $all.length) $checkAll.prop('checked',false);
                    else $checkAll.prop('checked',true);
                    
                    return false;
                } break;
                case 'uncheck-all': {
                    $allCheck.removeProp('checked');
                    $all.prop('checked',false);
                    $(extra).prop('checked',false);
                    $moderate.hide();
                    return false;
                } break;
            }
		}
    };

    function scrolling(pid, count)
    {
        var thisId = $('#j-sub-'+pid);
        var currentHeight = $(thisId).children('.j-sub-list').height();

        if($(thisId).scrollTop() >= (currentHeight - $(thisId).height()-20)){
            $('.r-sc').off('scroll');
            $(thisId).off('scroll');
            afterloadRegions(pid, count);
        }
    }

    function afterloadRegions(pid, count)
    {
        var thisId = $('#j-sub-'+pid);
        $(thisId).children('.j-sub-list').addClass('disabled');

        bff.ajax('<?= $this->adminLink('listing'); ?>'+'&act=regions&rp='+pid+'&rl='+count, {}, function(data){
            if(data) {
                $(thisId).children('.j-sub-list').html(data.regions);
            }
            $(thisId).children('.j-sub-list').removeClass('disabled');
        });
        $('.r-sc').on('scroll', function(){ scrolling(this.id, count+10); });
    }
}());
</script>