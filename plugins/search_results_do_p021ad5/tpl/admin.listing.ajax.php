<?php foreach($list as $k=>$v) { $id = $v['id']; ?>
	<tr class="view" data-id="<?= $id ?>">
    	<td class="checkbox">
            <label class="checkbox inline">
                <input type="checkbox" name="i[]" onclick="jItems.massModerate('check',this);" value="<?= $id ?>" class="check j-item-check" />
            </label>
        </td>
    	<td class="left">
    		<a class="linkout but" href="<?= BBS::url('items.search').'?q='.HTML::escape($v['query']); ?>" target="_blank"></a>
            <span id="q<?= $id ?>" class="copy j-plugin-fquerys-tooltip"<?php if(mb_strlen($v['query']) > 50) { ?> title="<?= HTML::escape($v['query']); ?>" data-toggle="tooltip" data-placement="right"<?php } ?>>
                <?= tpl::truncate($v['query'], 50, '...', true); ?></span>
    	</td>
    	<td>
    		<?= $v['counter']; ?>
    	</td>
    	<td>
    		<?= $v['counter'] - $v['hits']; ?>
    	</td>
    	<td>
    		<?= tpl::date_format3($v['last_time'], 'd.m.Y H:i'); ?>
    	</td>
    	<td>
    		<a class="but del item-del" href="#" rel="<?= $id ?>"></a>
        </td>
    </tr>
    
    <tr class="fold">
        <td colspan="6">
            <div class="r-sc" id="j-sub-<?= $id ?>">
                <table class="table table-hover table-striped j-sub-list" border="0" style="width: 100%; margin-bottom: 0;">
                </table>
            </div>
        </td>
    </tr>
<?php } if(empty($list)) { ?>
<tr class="norecords">
    <td colspan="6"><?= $this->langAdmin('Nothing found') ?></td>
</tr>
<?php } ?>