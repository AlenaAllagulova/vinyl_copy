<?php

use bff\db\migrations\Migration as Migration;

class ExtP021ad5e5be78e76eacd25a44cb1eda08014b6b1V1x0x0 extends Migration
{
    /**
     * Use this function to write migration.
     * Remember to use Table::update instead of Table::save
     */
    public function migrate()
    {
        if (!$this->hasTable($this->extension->table())) {
            $this->table($this->extension->table(), ['engine' => 'InnoDB', 'id' => false, 'primary_key' => ['id']])
                  ->addColumn('id', 'integer', ['signed' => false, 'identity' => true])
                  ->addColumn('pid', 'integer', ['signed' => false, 'null' => false, 'default' => 0])
                  ->addColumn('region_id', 'integer', ['signed' => false, 'null' => false, 'default' => 0])
                  ->addColumn('query', 'string', ['null' => false, 'default' => '', 'limit' => 80])
                  ->addColumn('counter', 'integer', ['signed' => false, 'null' => false, 'default' => 1])
                  ->addColumn('hits', 'integer', ['signed' => false, 'null' => false, 'default' => 0])
                  ->addColumn('last_time', 'datetime')
                  ->addIndex(['pid', 'region_id'], ['name' => 'idx_pid_region'])
                  ->addIndex(['pid', 'query'], ['name' => 'idx_pid_query'])
                  ->create();
        }
    }

    /**
     * Use this function to describe rollback actions
     * Remember to use Migration::dropIfExists instead of Table::drop
     */
    public function rollback()
    {
        Migration::dropIfExists($this->extension->table());
    }
}