<?php
/**
 * Системные настройки
 */

$config = array(
    'site.host'   => 'vinyl.com.ua',
    'site.static' => '//vinyl.com.ua',
    'site.title'  => 'Vinyl.com.ua', // название сайта, для подобных случаев: "Я уже зарегистрирован на {Vinyl.com.ua}"
    /**
     * Доступ к базе данных
     */
    'db.type' => 'mysql', // варианты: pgsql, mysql
    'db.host' => 'localhost', // варианты: localhost, ...
    'db.port' => '3306', // варианты: pgsql - 5432, mysql - 3306
    'db.name' => 'vinyl_comua',
    'db.user' => 'vinyl_comua',
    'db.pass' => 'lf9~E4x7CUZHKr',
    'db.charset' => 'UTF8',
    'db.prefix' => 'bff_',
    /**
     * Локализация
     * Подробности добавления дополнительных локализаций описаны в файле /install/faq.txt
     */
     'locale.available' => array( // список языков (используемых на сайте)
        // ключ языка => название языка
        'ru' => 'Русский',
        //'uk' => 'Українська',
        //'en' => 'English',
     ),
     'locale.default' => 'ru', // язык по-умолчанию
     //'locale.default.admin' => 'ru', // язык по-умолчанию в админ. панели
     'locale.hidden' => array( // языки скрытые от пользователей сайта
        //'en',
     ),
 

/**
 *  Mail
 */
'mail.support' => 'support@vinyl.com.ua',
'mail.noreply' => 'noreply@vinyl.com.ua',
'mail.admin' => 'admin@vinyl.com.ua',
'mail.fromname' => 'Vinyl',
'mail.method' => 'smtp',
'mail.smtp' => array(
    'host'=>'smtp.gmail.com',
    'port'=>587,
    'user'=>'admin@vinyl.com.ua',
    'pass'=>'UyaFGJzMK7Z8Qm',
    'secure'=>'tls',
    'debug'=>true,
),


/**
 *  SEO
 */
'seo.meta.limit.mdescription' => 300,

/**
 *  Images
 */

'bbs.items.images.width.min' => 220,
'bbs.items.images.height.min' => 220,
'bbs.search.premium.limit' => 3,

    /**
     *  Premium/last items
     */
    'bbs.index.premium.limit.init.from.phone' => 2, // Лимит инициализации премиул об. для моб. версии
    'bbs.index.premium.limit.load.from.phone' => 2, // Лимит ajax выгрузки премиул об. для моб. версии

    'bbs.index.last.limit.init.from.phone' => 20, // Лимит инициализации последних об. для моб. версии
    'bbs.index.last.limit.load.from.phone' => 20, // Лимит ajax выгрузки последних об. для моб. версии

    'bbs.index.premium.limit.load.from.desktop' => 4, // Лимит ajax выгрузки премиум об. для десктоп версии
    'bbs.index.last.limit.load.from.desktop' => 20, // Лимит ajax выгрузки последних об. для десктоп версии

    'bbs.search.premium.limit.init.from.phone' => 2, // Лимит инициализации премиум об. для моб. версии на странице поиска
    'bbs.search.premium.limit.load.from.phone' => 1, // Лимит ajax выгрузки премиум об. для моб. версии на странице поиска
    /**
     *  Items search
     */
    'bbs.search.quick.limit.phone' => 3, // Лимит количества об. в подсказках быстрого поиска для моб. версии
    /**
     *  Dyn props base group
     */
    'bbs.base.dyn.prop.simple.item' => false, // в подаче ОБ от частного лица использовать дин.св-ва базовой группы (значение 0 для дин.св-в базовой категории) (варианты: true|false)
    'bbs.base.dyn.prop.shop.item'   => true,  // в подаче ОБ от магазина использовать дин.св-ва базовой группы (варианты: true|false)
    'bbs.base.dyn.prop.simple.collapse' => false, // в подаче ОБ от частного лица оборачивать в Collapse дин.св-ва базовой группы (варианты: true|false)
    'bbs.base.dyn.prop.shop.collapse' => true,   // в подаче ОБ от магазина оборачивать в Collapse дин.св-ва базовой группы (варианты: true|false)
    /**
     *  Dyn props group publisher in category
     */
    'bbs.publisher.dyn.prop.simple.collapse' => true, // в подаче ОБ от частного лица оборачивать в Collapse дин.св-ва группы "simple" (варианты: true|false)
    'bbs.publisher.dyn.prop.shop.collapse' => true,   // в подаче ОБ от магазина оборачивать в Collapse дин.св-ва группы "shop"  (варианты: true|false)
    /**
     *  Shop settings
     */
    'shop.view.message.send' => false, // возможность отправлять сообщения влвдельцу магазина из карточки магазина
    /**
     *  SEO settings
     */
    'bbs.item.view.meta.description.limit' => 300, // Кол-во символов в описании объявления для meta description для карточки объявления  (варианты: false|0 - не используем лимитирование, int N - обрезаем описание ОБ по N символов)

   /**
     * Debug (для разработчика)
     */
    'php.errors.reporting' => -1, // all
    'php.errors.display'   => 1, // отображать ошибки (варианты: 1|0)
    'debug' => true, // варианты:true|false - включить debug-режим
    /**
     * Дополнительные настройки:
     * ! Настоятельно не рекомендуется изменять после запуска проекта
     */
    'date.timezone' => 'Europe/Kiev', // часовой пояс
    'config.sys.admin' => true, // Возможность редактирования большей части системных настроек через админ. панель в "режиме разработчика"
    'site.static.minify' => true, // Минимизация файлов статики: js, css
    # SEO
    'seo.landing.pages.enabled' => true, // Задействовать посадочные страницы (варианты: true|false)
    'seo.landing.pages.fields'  => array(
        'titleh1' => array(
            't'=>'Заголовок H1',
            'type'=>'text',
        ),
        'seotext' => array(
            't'=>'SEO текст',
            'type'=>'wy',
        ),
    ),
    'seo.redirects' => true, // Задействовать редиректы (варианты: true|false)
    'geo.filter.url' => true, // Фильтр по региону из указанного в URL
    'device.desktop.responsive' => true, // Responsive для desktop версии сайта (false - выключен)
    # Хуки
    'hooks' => array(
        # Контакты (поля ввода)
        # Выключая уже используемые поля вы скрываете/удаляете контактные данные указанные пользователями ранее
        'users.contacts.fields' => function($list) {
            return config::merge($list, array(
                'skype'    => ['enabled' => 0],
                'icq'      => ['enabled' => 0],
                'whatsapp' => ['enabled' => 0],
                'viber'    => ['enabled' => 0],
                'telegram' => ['enabled' => 0],
                'example'  => [ # Ключ должен быть уникальным и содержать символы a-z
                    'title'    => _te('', 'Vinyl.com.ua title'),
                    'icon'     => 'fa fa-comment', # http://fontawesome.io/icons/
                    'priority' => 1,
                    'enabled'  => false, # включен - true, выключен - false
                ],
            ));
        },
        # Системы оплаты доступные пользователю:
        # 'enabled' => true, # включено
        # 'enabled' => false, # выключено
        # currency_id - ID валюты в разделе "Настройки сайта / Валюты"
        'bills.pay.systems.user' => function($list, $extra) {
            $list = config::merge($list, array(
                'robox' => array( # Robokassa
                    'enabled' => false,
                    'title'   => _t('bills', 'Robokassa'),
                ),
                'wm' => array( # Webmoney WMZ
                    'enabled' => false,
                    'title'   => _t('bills', 'Webmoney'),
                ),
                'wmr' => array( # Webmoney WMR
                    'enabled' => false,
                    'title'   => _t('bills', 'Webmoney WMR'),
                    'logo_desktop' => $extra['logoUrl'] . 'wm.png',
                    'logo_phone'   => $extra['logoUrl'] . 'wm.png',
                    'way'     => 'wmr',
                    'id'      => Bills::PS_WM,
                    'currency_id' => 2, # рубли
                ),
                'wmu' => array( # Webmoney WMU
                    'enabled' => false,
                    'title'   => _t('bills', 'Webmoney WMU'),
                    'logo_desktop' => $extra['logoUrl'] . 'wm.png',
                    'logo_phone'   => $extra['logoUrl'] . 'wm.png',
                    'way'     => 'wmu',
                    'id'      => Bills::PS_WM,
                    'currency_id' => 1, # гривны
                ),
                'terminal' => array( # W1
                    'enabled' => false,
                    'title'   => _t('bills', 'Терминал'),
                ),
                'paypal' => array( # PayPal
                    'enabled' => false,
                    'title'   => _t('bills', 'Paypal'),
                ),
                'liqpay' => array( # LiqPay
                    'enabled' => false,
                    'title'   => _t('bills', 'Liqpay'),
                ),
                'yandex' => array( # Yandex.Деньги
                    'enabled' => false,
                    'title'   => _t('bills', 'С кошелька'),
                ),
                'yandexAC' => array( # Yandex.Деньги
                    'enabled' => false,
                    'title'   => _t('bills', 'Банковская карта'),
                ),
            ));
            return $list;
        },
        # Блок премиум/последних на главной:
        'bbs.index.last.blocks' => array(
            'premium', 'last',
        ),
        # Расширение текстовых полей дин. свойств:
        'bbs.dp.settings' => array(
            'datafield_text_last'  => 30,
        ),
    ),
);

if (file_exists(PATH_BASE.'config'.DIRECTORY_SEPARATOR.'sys-local.php')) {
    $local = include 'sys-local.php';
    return array_merge($config, $local);
}


return $config;