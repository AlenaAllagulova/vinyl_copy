<?php

$dp = BBS::i()->dpForm(BBSBase::CATS_ROOTID, false, [], ['only_base_group' => true, 'without_style' => true], []);
?>
<form action="<?=Users::url('base_dp')?>" method="post">
    <?=$dp;?>
    <input type="submit"
           class="btn btn-danger"
           value="<?=_t('base_dp', 'Сохранить для всех объявлений')?>"
           onclick="return confirm('Вы уверены? Выбранные значения будут активированы для всех объявлений')">
</form>
