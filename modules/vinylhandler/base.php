<?php

abstract class VinylhandlerBase extends \Module
{
    /** @var VinylhandlerModel */
    public $model = null;
    public $securityKey = 'd5aa7305a35f0d3228b0fb1c7c34dda9';

    const DYN_PROP_BASE_GROUP = '0'; # группа дин. свойств связанная с базовой категории, в базе хранится строковое значение
    const DYN_PROP_GROUP_SIMPLE = 'simple';
    const DYN_PROP_GROUP_SHOP = 'shop';
    const DYN_PROP_GROUP_SIMPLE_SHOP = 'simple-shop'; //simple-shop
    
    const DYN_PROP_SHOW_IN_SHOP = 1;
    const DYN_PROP_SHOW_IN_SIMPLE = 2;
    const DYN_PROP_SHOW_IN_SIMPLE_SHOP = 3;

    # дин. свойства базовой категории группы "0"
    const DYN_PROP_TYPE_BASE = 'base';
    # дин. свойства в категории (не наследуемые от базовой категории),
    # входящие или в группу "simple" или в группу "shop", не включают в себя дин. св-ва других групп или не указанной группы
    const DYN_PROP_TYPE_PUBLISHER = 'publisher';

    const PUBLISHER_TYPES = [
        self::DYN_PROP_SHOW_IN_SHOP        => self::DYN_PROP_GROUP_SHOP, 
        self::DYN_PROP_SHOW_IN_SIMPLE      => self::DYN_PROP_GROUP_SIMPLE, 
        self::DYN_PROP_SHOW_IN_SIMPLE_SHOP => self::DYN_PROP_GROUP_SIMPLE_SHOP,
    ];
    
    public function init()
    {
        parent::init();
        //
    }
    
    /**
     * @return VinylhandlerModel
     */
    public static function model()
    {
        return bff::model('Vinylhandler');
    }
    
    /**
     * @return Vinylhandler
     */
    public static function i()
    {
        return bff::module('Vinylhandler');
    }

    public static function getLastPremiumSimpleBlock()
    {
        $aData['last'] = '';
        foreach (bff::filter('bbs.index.last.blocks', array(false)) as $v) {
            $aData['last'] .= BBS::i()->indexLastBlock($v, [ 'tpl' => 'index.last.simple.block']);

        }
        return $aData['last'];
    }

    /**
     * Check item add page
     * @return bool
     */
    public static function isItemAddPage()
    {
        if ( (boolean)strstr(Request::getSERVER('REQUEST_URI'), 'item/add')){
            return true;
        }
        return false;

    }

    /**
     * Выводить ли дин св-ва группы "0" для объявлений от частного лица(простое ОБ) / от магазина
     * @param $sType
     * @return mixed|void
     */
    public static function isUseBaseDynPropInItemType($sType)
    {
        if (!$sType || !in_array($sType, self::PUBLISHER_TYPES)){
            return ;
        }
        return config::sys('bbs.base.dyn.prop.'.$sType.'.item', true);
    }

    /**
     * Оборачивать ли в Collapse дин св-ва группы "0" для объявлений от частного лица(простое ОБ) / от магазина
     * @param $sType
     * @return mixed|void
     */
    public static function isCollapseBaseDynPropInItemType($sType)
    {
        if (!$sType || !in_array($sType, self::PUBLISHER_TYPES)){
            return ;
        }
        return config::sys('bbs.base.dyn.prop.'.$sType.'.collapse', true);
    }

    /**
     * Оборачивать ли в Collapse дин св-ва, входящие или в группу "simple" или в группу "shop", для объявлений от частного лица(простое ОБ) / от магазина
     * @param $sType
     * @return mixed|void
     */
    public static function isCollapsePublisherDynPropInItemType($sType)
    {
        if (!$sType || !in_array($sType, self::PUBLISHER_TYPES)){
            return ;
        }
        return config::sys('bbs.publisher.dyn.prop.'.$sType.'.collapse', true);
    }

    /**
     * Возможность отправлять сообщения влвдельцу магазина
     * @return mixed
     */
    public static function isShopViewMessageSend()
    {
        return config::sys('shop.view.message.send', true);
    }

    /**
     * Быстрый поиск на любом типе устроййств (false - только desktop)
     * @return mixed
     */
    public static function isQuickSearchOnAnyDevice()
    {
        return config::sys('bbs.quick.search.any.device', true);
    }

    /**
     * Seo настройки
     * Кол-во символов в meta description для карточки объявления
     * @return mixed (ex 0: не используем лимитирование, int: default 150)
     */
    public static function metaDescrItemViewLimit()
    {
        return config::sys('bbs.item.view.meta.description.limit', 150);

    }

    /**
     * Получаем последний выбор настройки "доставка в регионы"
     * @return mixed
     */
    public static function getLastRegDelivery()
    {
        $data = BBS::model()->itemsList(['user_id' => User::id()], false, ['orderBy' => 'id DESC',
            'limit' => 1,]);

        if (empty($data)) {
            return null;
        } else {
            $data = reset($data);
            return $data['regions_delivery'];
        }
    }
}