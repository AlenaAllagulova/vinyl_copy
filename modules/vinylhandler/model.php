<?php

class VinylhandlerModel extends \Model
{
    /** @var VinylhandlerBase */
    public $controller;
    
    public function getLastUserItem()
    {
        $sql = "SELECT * FROM ".TABLE_BBS_ITEMS." WHERE user_id = :user_id ORDER BY id DESC LIMIT 1";
        $bind = [':user_id' => User::id()];
        
        return $this->db->one_array($sql, $bind);
    }
    
    public function getSelectedCategoryData($selectedCategoryId)
    {
        $sql = "
            SELECT
                year_enabled,
                year_title,
                year_show_in
            FROM 
                ".TABLE_BBS_CATEGORIES." cat
                JOIN ".TABLE_BBS_CATEGORIES_LANG." cat_lang ON (
                    cat_lang.id = cat.id AND 
                    cat_lang.lang = '".LNG."')
            WHERE cat.id = :id";
        $bind = [':id' => $selectedCategoryId];
    
        return $this->db->one_array($sql, $bind);
    }
}