<?php

class Vinylhandler extends VinylhandlerBase
{

    public function init()
    {
        parent::init();
    }

    private static function getBbsItemsLimit($type, $state, $page = 'index')
    {
        $keyName = "bbs.$page.$type.limit.$state.from." . (DEVICE_PHONE ? bff::DEVICE_PHONE : bff::DEVICE_DESKTOP);
        if ( ! DEVICE_PHONE && $state == 'init') {
            return config::sysTheme("bbs.index.$type.limit", 10, TYPE_UINT);
        }
        return config::sys($keyName, 0, TYPE_UINT);
    }

    public static function getBbsItemsLimitInit($type, $page = 'index')
    {
        return self::getBbsItemsLimit($type, 'init', $page);
    }

    public static function getBbsItemsLimitLoad($type, $page = 'index')
    {
        return self::getBbsItemsLimit($type, 'load', $page);
    }

    public function ajax()
    {
        $userId = User::id();
        $response = [];

        switch ($this->input->getpost('act', TYPE_STR)) {
            case 'get-bbs_items':

                $offset = $this->input->getpost('offset', TYPE_INT);
                $order = 'publicated_order DESC, id DESC';

                $type = $this->input->getpost('type', TYPE_STR);
                $page = $this->input->getpost('page', TYPE_STR);
                $cat_id = $this->input->getpost('cat_id', TYPE_INT);
                $limit = self::getBbsItemsLimitLoad($type, $page);

                $BBS = BBS::i();
                $filter = [
                    'is_publicated' => true,
                    'status' => BBS::STATUS_PUBLICATED
                ];

                switch ($type) {
                    case 'premium':
                        $order = 'svc_premium_order DESC, id DESC';
                        if (config::sysAdmin('bbs.index.premium.rand', false, TYPE_BOOL)) {
                            $order = 'RAND()';
                        }
                        $filter['svc'] = ['>', 0];
                        $filter['svc_premium'] = true;
                        if ($page == 'search' && !empty($cat_id) && config::sysAdmin('bbs.search.premium.category', true, TYPE_BOOL)) {
                            $filter[':cat-filter'] = $cat_id;
                        }
                    break;
                    case 'last':
                        $filter[':not_svc_premium'] = 'svc_premium != 1';
                    break;
                }

                $total = $BBS->model->itemsList(
                    $filter,
                    true,
                    ['context' => 'search']
                );
                if ($type == 'premium' && $page == 'search') {
                    $configCnt = config::sysAdmin('bbs.search.premium.limit', 0, TYPE_UINT);
                    $total = ($total >= $configCnt ? $configCnt : $total);
                }

                $items = $data['items'] = [];
                if ($offset < $total) {
                    $items = $data['items'] = $BBS->model->itemsList(
                        $filter,
                        false,
                        [
                            'context' => 'last-block',
                            'limit'   => $limit,
                            'offset'  => $offset,
                            'orderBy' => $order,
                            'convertCurrency' => true,
                        ]
                    );
                }

                $response['data'] = [
                    'offset' => ($offset + $limit >= $total) ? 0 : $offset + $limit,
                    'content' => '',
                ];

                if ($type == 'premium' && $page == 'search') {
                    $BBS->itemsListPrepare($data['items'], BBS::LIST_TYPE_GALLERY);
                    $data['ajax_load'] = true;
                    $response['data']['content'] = $BBS->viewPHP($data, 'search.premium.block.ajax');
                } else {
                    foreach ($items as &$v) {
                        $response['data']['content'] .= View::template(
                            'search.item.gallery',
                            [
                                'item' => &$v
                            ],
                            'bbs'
                        );
                    } unset ($v);
                }

                break;
            default:
                $this->errors->impossible();
                break;
        }

        $this->ajaxResponseForm($response);
    }

}